/*
    盛读小说
    支持多账号
    url为daily/welfareV3或user/my/tab 全地址
    变量 ryddjxsapp='[{"os":"ios","url":"xxx","cookie":"xxx"},{"os":"android","url":"xxx","cookie":"xxx"}]'

    cron:15 6-23 * * *
 */

const $ = new Env('盛读小说');
let djxsapp = $.isNode() ? (process.env.shengduxsapp ? process.env.shengduxsapp : "") : ($.getdata('shengduxsapp') ? $.getdata('shengduxsapp') : "")

const notify = $.isNode() ? require('./sendNotify') : '';

const crypto = require("crypto");
const zlib = require("zlib");
const cheerio = require('cheerio');
const vm = require("vm");

let privateKey = '-----BEGIN PRIVATE KEY-----\n' +
    'MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAMXGjyS3p+3AVnlBJe5VQ6tC9inh8tVBve4r+yBjC5HQD6th2n3tSyuNVYaNRAFSEq+OENwnwwhjbYUnjLWb+qZscB43K1+4/WlKdvfgwQVXm0ZQ2+jMBf+165UBEEuuWT2WqXeKkkUqPQta5lrt4eFfbo53JcOO4D5fDSGQS5bZAgMBAAECgYAor4I/AXEQXeLsKtTMxMmY77uIPi0gZdfWqUGOFhIJOw4eKZEzGp++I+MWPPVieCnT55vcTmm2zg13uP0fVykmukWqZszG/ZNpPKYleOqnZOqQj7O3au8Ywz18F/pqD++PsUzxRVeXxSOOwmjQ0D2Pe/9yutz62pyiFGAzDsaI6QJBAMn8DeBT3AtcWuONdiHL3yC4NkGJDdyBbMOaWyvrcvUUZr13uS9mZO6pLTN6v9tkmPUdvYxcPTJ9wdGR7NcNPDsCQQD6qluGI2VAlz4s5UoDnelFKrwDPeiruE3I6wsrasK6h37DsAE6OrQgx2dm4yH7ntJHUlJCZ5ay1EBNfEexgQv7AkA1r2vUwxVKY7q4nqHWa8SbgrrRAmePw0qwVreC3erJHyoLk+XBpnqPQKIF+8tAueU5yTTXOLD/WZOJazrDEf5/AkBpwG+Ggu5Xtrcbd8ynA/sDHElf0MGVmNbwOgFnWs42pa1cX6fU6ilOXvIH3TFcF6A9SMS9kThpz9QlHJaek4P7AkAavQillA/wnrha9GsK5UFmzmwNfkjLLW4psAUsXOsqFXWMoxTd0xWuSbuVOzERpbFMBl1VoZQmD9BLSVOTNe+v' +
    '\n-----END PRIVATE KEY-----'
let readHD = {
    'Host': "dj.palmestore.com",
    "user-agent": 'okhttp/3.11.0',
    "Content-Type": "text/plain",
    "Accept-Encoding": "gzip",
}
let allmsg = httpResult = ''
let debuglog = false

class Account {
    constructor(os, usr, zyeid, uri, cookie, nickname, p2) {
        this.usr = usr
        this.zyeid = zyeid
        this.uri = uri
        this.cookie = cookie
        this.nickname = nickname
        this.time = 0
        this.os = os
        this.readReward = false
        this.p2 = p2
        this.alipay = false
        this.new = false
    }
}!(async () => {
    if (typeof $request !== "undefined") {

    } else {

        
        // let signuri = `timestamp=1692230172052&usr=j169104443&param=43&p2=124293` // 签到通知
        // let signdata = sortUri(signuri)
        // console.log(createSign(signdata))

        djxsapp = toObj(djxsapp)
        let accarr = []
        for (let i = 0; i < djxsapp.length; i++) {
            let uri = getUri(djxsapp[i].url)
            let cookie = djxsapp[i].cookie
            let os = djxsapp[i].os
            let usr = getQueryVariable(uri, 'usr')
            let zyeid = getQueryVariable(uri, 'zyeid')
            let p2 = getValue(uri, 'p2')
            accarr.push(new Account(os, usr, zyeid, uri, cookie, '', p2))
        }



        console.log(`共${accarr.length}个账号`)

        for (let i = 0; i < accarr.length; i++) {
            
            console.log(`第${i+1}个账号`)

            await getAccount(accarr[i])
            if (new Date().getHours() > 1) {
                await withdrawPage(accarr[i])

            }
            await indexRefreshV5(accarr[i])
            await dailyWelfareV5(accarr[i], 3)
            // if(accarr[i].new) continue
            await dailyWelfareV5(accarr[i])
            await dailyWelfareV5(accarr[i], 2)

            console.log('----------------------------')
        }



        for (let j = 0; j < accarr.length; j++) {
            moneyChallenge(accarr[j]);
        }
        await $.wait(30000 * 10)

        accarr = accarr.filter(a => {
            return a.time < 240
        })

        if (accarr.length > 0) {
            for (let j = 0; j < 40; j++) {
                if (accarr.length === 0) {
                    break
                }
                for (let i = 0; i < accarr.length; i++) {
                    readTimer(accarr[i])
                }
                await $.wait(50000)
            }
        }
    }
})()
.catch((e) => $.logErr(e))
    .finally(() => $.done())

async function receiveV5(account, url, signparam) {
    let sign = createSign(sortUri(signparam))
    let u = `https://dj.palmestore.com/zycl/gold/receiveV5?${url}&${signparam}`
    let opt = populateUrlObject(`${u}&sign=${sign}`, account.cookie)
    if (account.os === 'ios') {
        opt = iosPopulateUrlObject(`${u}&sign=${sign}`, account.cookie)
    }
    let res = await get(opt)
    return res
}
async function noticeVideoV5(account, url, signparam) {
    let sign = createSign(sortUri(signparam))
    let u = `https://dj.palmestore.com/zycl/gold/noticeVideoV5?${url}&${signparam}`
    let opt = populateUrlObject(`${u}&sign=${sign}`, account.cookie)
    if (account.os === 'ios') {
        opt = iosPopulateUrlObject(`${u}&sign=${sign}`, account.cookie)
    }
    let res = await get(opt)
    return res
}

async function _withdrawalByVideo(account, url, signparam) {
    let sign = createSign(sortUri(signparam))
    let u = `https://dj.palmestore.com/zycl/gold/withdrawalByVideo?${url}&${signparam}`
    let opt = populateUrlObject(`${u}&sign=${sign}`, account.cookie)
    if (account.os === 'ios') {
        opt = iosPopulateUrlObject(`${u}&sign=${sign}`, account.cookie)
    }
    let res = await get(opt)
    return res
}

function getValue(str, key, split = '&') {
    let reg = new RegExp(`${key}=([^${split}]*)`)
    let res = reg.exec(str)
    return res[1]
}
async function openbox(account, param = 50) { //安卓50 ios 51
    let ts = getTimestamp()
    if (account.os === 'ios') {
        ts = iosTS()
        param = 51
    }
    let signparam = `timestamp=${ts}&usr=${account.usr}&param=${param}&p2=${account.p2}`
    let res = await receiveV5(account, `${account.uri}&type=9`, signparam)
    if (res.code === 0) {
        console.log(`【${account.nickname}】开宝箱成功，获得${res.body.coin}金币,已开${res.body.count}次宝箱，冷却时间： ${formatTime(res.body.endTime)}`)
        if (res.body.count < 5)
            await videoV5(account, 10182, 'VIDEO_POP_WINDOW')
    } else if (res.code === 40001) {
        console.log(`【${account.nickname}】开宝箱失败冷却中，冷却时间： ${formatTime(res.body.endTime)}`)
    } else {
        console.log(`【${account.nickname}】开宝箱失败 ${toJSON(res)}`)
    }
}
async function videoV5(account, param, pos) {
    let ts = getTimestamp()
    if (account.os === 'ios') {
        ts = iosTS()
    }
    let signparam = `timestamp=${ts}&usr=${account.usr}&param=${param}&p2=${account.p2}`
    let res = await noticeVideoV5(account, `${account.uri}&pos=${pos}`, signparam)
    // console.log(res)
    if (res.code === 0) {
        console.log(`【${account.nickname}】${param} 广告视频观看成功！获得${res?.body?.goldNum}金币`)
    } else {
        console.log(`【${account.nickname}】${param} 广告视频观看失败，${toJSON(res)}`)
    }
}
//三餐任务，一次性任务
async function commonTask(account, type, param, coin) {
    let ts = getTimestamp()
    if (account.os === 'ios') {
        ts = iosTS()
    }
    let signparam = `timestamp=${ts}&usr=${account.usr}&param=${param}&p2=${account.p2}`
    let res = await receiveV5(account, `${account.uri}&type=${type}&coin=${coin}`, signparam)
    if (res.code === 0) {
        console.log(`【${account.nickname}】任务完成成功`)
    } else if (res.code === 40002) {} else {
        console.log(`【${account.nickname}】任务完成失败 ${toJSON(res)}`)
    }
}

async function withdrawVideo(account, id, position) {
    let param = `${id}-${position}-0`
    let ts = getTimestamp()
    if (account.os === 'ios') {
        ts = iosTS()
    }
    let signparam = `timestamp=${ts}&usr=${account.usr}&param=${param}&p2=${account.p2}`
    let res = await _withdrawalByVideo(account, `${account.uri}&position=${position}&id=${id}&itemId=0&smboxid`, signparam)
    // console.log(res)
    if (res.code === 0) {
        console.log(`【${account.nickname}】提现视频观看成功！`)
        return res
    } else {
        console.log(`【${account.nickname}】提现视频观看失败，${toJSON(res)}`)
    }
}


async function videoTask(account, param, type) {
    let ts = getTimestamp()
    if (account.os === 'ios') {
        ts = iosTS()
    }
    let signparam = `timestamp=${ts}&usr=${account.usr}&param=${param}&p2=${account.p2}`
    let res = await receiveV5(account, `${account.uri}&type=${type}`, signparam)
    if (res.code === 0) {
        console.log(`【${account.nickname}】商业视频任务完成成功，获得${res.body.coin}金币，还剩${res.body.remainingCount}`)
    } else if (res.code === 40002) {} else {
        console.log(`【${account.nickname}】商业视频任务完成失败 ${toJSON(res)}`)
    }
}

async function readReward(account, id, coin) {
    let ts = getTimestamp()
    if (account.os === 'ios') {
        ts = iosTS()
    }
    let signparam = `timestamp=${ts}&usr=${account.usr}&param=742-${id}&p2=${account.p2}`
    let sign = createSign(sortUri(signparam)).replaceAll('+', ' ')
    let u = `https://dj.palmestore.com/zycl/gold/receiveV5?${account.uri}&${signparam}&sign=${sign}&type=1&coin=${coin}&smboxid=`
    let opt = populateUrlObject(u)
    if (account.os === 'ios') {
        opt = iosPopulateUrlObject(u)
    }
    let res = await get(opt)
    if (res.code === 0) {
        console.log(`【${account.nickname}】阅读奖励领取成功，获得${res.body.coin}`)
    } else {
        console.log(`【${account.nickname}】阅读奖励领取失败 ${toJSON(res)}`)
    }
}
//提现广告



async function dailyWelfareV5(account, signType = 1) {
    let u = `https://dj.palmestore.com/zycl/gold/dailyWelfareV5?${account.uri}&source=welfare&signType=${signType}&ecpmMix=0.0&ecpmVideo=10.01&mcTacid=`
    let opt = populateUrlObject(u, account.cookie)
    if (account.os === 'ios') {
        opt = iosPopulateUrlObject(u, account.cookie)
    }
    let res = await get(opt)
    if (res.code === 0) {
        //新用户30天
        if (signType === 3) {
            if (Object.prototype.toString.call(res.body.signThirtyGiftBag) === '[object Object]') {
                account.new = true
                //新用户签到
                if (res.body.signThirtyGiftBag.currentDay > res.body.signThirtyGiftBag.count) {
                    await thirtyGiftSignin(account)
                }
                let params = {
                    "productId": 1,
                    "subId": 1,
                    "geartype": 'thirty_gift',
                    "money": 0.3,
                    "coin": 0,
                    "days": 1
                }

                //新用户提现
                if (account.alipay) {
                    let dlist = res.body.signThirtyGiftBag.list
                    for (let day of dlist) {
                        if (res.body.signThirtyGiftBag.currentDay === day.days && day.drawStatus === 0) {
                            params.days = res.body.signThirtyGiftBag.currentDay
                            params.money = day.amount
                            await withdrawalPopupInit(account, params)
                            break
                        }
                    }
                }
                let continueList = res.body.signThirtyGiftBag.continueList
                for (let el of continueList) {
                    if (res.body.signThirtyGiftBag.currentDay >= el.days && el.drawStatus === 0) {
                        params.days = el.days
                        params.money = el.amount
                        await withdrawalPopupInit(account, params)
                        break
                    }
                }

            }
        }
        if (res.body.hasOwnProperty('signNormal')) {


            if (!res.body.signNormal.isTodayFirstSign) {
                console.log(`【${account.nickname}】已签到${res.body.signNormal.continueDay}天`)
                console.log(`【${account.nickname}】${res.body.signNormal.desc}`)
            } else {
                console.log(`已签到${res.body.signNormal.continueDay}天`)
                console.log(`${res.body.signNormal.title}`)
                console.log(`${res.body.signNormal.desc}`)
                let signparam = `timestamp=${getTimestamp()}&usr=${account.usr}&param=10562&p2=${account.p2}`
                if (account.os === 'ios') {
                    let r1 = await noticeVideoV5(account, `${account.uri}&pos=VIDEO_ALIPAY_SIGN_IN`, signparam)

                    let r2 = await withdrawVideo(account, 10562, 'VIDEO_ALIPAY_SIGN_IN')
                    if (r1.code === 0 && r2.code === 0) {
                        console.log(`【${account.nickname}】签到奖励领取成功`)
                    } else {
                        console.log(`【${account.nickname}】签到奖励领取失败,r1 ${toJSON(r1)} ,r2 ${toJSON(r2)}`)
                    }
                } else {

                }
            }
        }

        //新用户7天
        if (res.body.hasOwnProperty('signGiftBag') && Object.prototype.toString.call(res.body.signGiftBag) === '[object Object]') {
            for (let d of res.body.signGiftBag.list) {
                if (d.formatDays === '今日') { //1 领取 2 过期 3 可领取 4
                    console.log(`${res.body.signGiftBag.titleV2} ${res.body.signGiftBag.subTitle} ${res.body.signGiftBag.desc}`)
                    if (d.type === 4) { //现金
                        let params = {
                            type: 1,
                            coin: 0,
                            money: 0.3,
                            productId: 20,
                            subId: 20,
                            geartype: 'giftbag',
                            days: 1
                        }
                        await giftBag(account, d.days, 4)
                        await withdrawalPopupInit(account, params, 1)
                    } else {
                        await giftBag(account, d.days)
                    }
                    break
                }
            }
        }



        for (let task of res.body.taskInfo.taskGroups) {
            if (task.name === '阅读任务') {
                account.time = task.list[0].readTime;
                let config = task.list[0].config
                for (let el of config) {
                    if (el.status === 2 && el.isCash === 0) {
                        console.log(`【${account.nickname}】领取${el.time}分钟阅读奖励`)
                        await readReward(account, el.id, el.coin)
                    } else if(el.status === 2){
                        let signparam = `timestamp=${getTimestamp()}&usr=${account.usr}&param=880-${el.id}&p2=${account.p2}`
                        await receiveV5(account, `${account.uri}&type=1&coin=0`, signparam)
                    }
                }
                continue
            }

            if (task.name === "商业化任务" && signType === 1) {
                for (let e of task.list) {
                    if (e.name.indexOf("看视频赚金币与声望") > -1) {
                        if (e.usedCount < 10) {
                            await videoTask(account, e.id, e.type)
                            // await videoTask(account,'10348-1',1002)
                        }
                    } else if (e.name === '看视频秒提支付宝红包') {
                        if (e.usedCount < e.totalCount) {

                            if (account.alipay) {
                                let signparam = `timestamp=${getTimestamp()}&usr=${account.usr}&param=11126&p2=${account.p2}`
                                let r1 = await noticeVideoV5(account, `${account.uri}&pos=VIDEO_ALIPAY_RED_PKG`, signparam)
                                await $.wait(5000)
                                let r2 = await withdrawVideo(account, 11126, 'VIDEO_ALIPAY_RED_PKG')
                                if (r1.code === 0 && r2.code === 0) {
                                    console.log(`【${account.nickname}】看视频秒提支付宝红包领取成功`)
                                } else {
                                    console.log(`【${account.nickname}】看视频秒提支付宝红包失败,r1 ${toJSON(r1)} ,r2 ${toJSON(r2)}`)
                                }
                            }
                        }
                    }
                }
            }

            // if (task.name === '新手任务' && signType === 1){
            //     for (let xs of task.list) {
            //         if(xs.status === 2){ //1 完成 2未完成
            //             await commonTask(account,xs.type,xs.id,xs.coin)
            //         }
            //     }
            // }

        }
        //宝箱
        if (res.body.taskInfo.boxTask.countDown === 0) {
            await openbox(account)

        }
        if (res.body.taskInfo.quickTask.length > 0) {
            for (let task of res.body.taskInfo.quickTask) {
                if (task.type === 13) {
                    await commonTask(account, task.type, task.id, task.coin)
                }
            }
        }

    } else {
        console.log(`获取任务状态失败，${toJSON(res)}`)
    }
}

async function withdrawPage(account) {
    let url = `https://dj.palmestore.com/zycl/gold/withdraw?${account.uri}`
    let opt = populateUrlObject(url)
    let res = await get(opt)
    let data = cheerio.load(res)
    let jsCode = ''

    data('script').each((i, s) => {
        if (data(s).text().indexOf('videoUsedCount') > -1) {
            jsCode = data(s).text()
        }
    })
    let context = runJavaScript(jsCode)

    let wechatState = context.bindStatusWeixin
    let alipayState = context.bindStatusAlipay
    let videoUsedCount = context.videoUsedCount
    let todayReadTime = context.todayReadTime
    let userGold = context.userGold
    let bindInfo = context.bindInfo
    let myContinueLoginDays = context.myContinueLoginDays
    if(bindInfo.preType === 'alipay'){
        let aliname = base64Decrypt(bindInfo.aliName)
        console.log(`【${account.nickname}】支付宝绑定账号：${aliname}`)
        account.alipay = true
        account.bindInfo = bindInfo
    }else if(bindInfo.preType === 'wechat'){
        let wechatname = base64Decrypt(bindInfo.wechatName)
        console.log(`【${account.nickname}】微信绑定账号：${wechatname}`)
        account.wechat = true
        account.bindInfo = bindInfo
    }else{
        console.log(`【${account.nickname}】未绑定提现账号`)
    }
    
    // console.log(`【${account.nickname}】连续登录天数：${myContinueLoginDays}\n阅读时长：${todayReadTime}\n观看视频广告次数：${videoUsedCount}\n金币：${userGold}\n微信绑定：${wechatState?'已绑定':'未绑定'}，微信绑定：${alipayState?'已绑定':'未绑定'}`)


}
function base64Decrypt(str) {
    return Buffer.from(str, 'base64').toString();
}
function base64Encrypt(str) {
    return Buffer.from(str).toString('base64');
}
async function giftBag(account, day, type = 2) {
    let url = `https://dj.palmestore.com/zycl/giftbag/receive?${account.uri}&days=${day}&incrId=17&giftSource=speed&type=${type}`
    let opt = populateUrlObject(url, account.cookie)
    let res = await get(opt)
    if (res.code === 0) {
        console.log(`【${account.nickname}】新人7天奖励领取成功`)
    } else {
        console.log(`【${account.nickname}】新人7天奖励领取失败，`, toJSON(res))
    }

}

async function thirtyGiftSignin(account) {
    let ts = getTimestamp();
    let signstr = `timestamp=${ts}&usr=${account.usr}&param=1-0&p2=${account.p2}`
    let sign = createSign(sortUri(signstr)).replaceAll('+', ' ')
    let url = `https://dj.palmestore.com/zycl/thirtyGift/sign?${account.uri}&param=1-0&sign=${sign}&timestamp=${ts}`
    let opt = populateUrlObject(url)
    let res = await get(opt)
    if (res.code === 0) {
        console.log(`【${account.nickname}】新用户签到成功`)
    } else {
        console.log(`【${account.nickname}】新用户签到失败，${toJSON(res)}`)
    }
}

async function withdrawalPopupInit(account, params, type = 6) {
    let url = `https://dj.palmestore.com/zycl/gold/withdrawalPopupInit?${account.uri}&type=${type}`
    if (type === 6) {
        url += `&taskId=0&days=${params.days}&signType=1&from=welfare`
    }
    let opt = populateUrlObject(url)
    let res = await get(opt)
    if (res.code === 0) {
        let user = res.body.bindInfo;
        // let user = account.bindInfo
        if (params.days > 0) {
            if (user.aliId !== '' && user.aliName !== null) {
                await withdrawSubmitNoCheckCode(account, user.aliId, user.aliName, params)
            } else if (user.wechatId !== '' ) {
                await withdrawSubmitNoCheckCode(account, user.wechatId, user.wechatName, params, 'weixin')
            } else {
                console.log(`【${account.nickname}】没有绑定任何提现方式`)
            }
        }
        return user
    } else {
        console.log(`【${account.nickname}】提现初始化失败`, toJSON(res))
    }
}

function formatObjectToString(obj) {
    return Object.keys(obj)
        .map(key => `${key}=${obj[key]}`)
        .join('&');
}




async function withdrawSubmitNoCheckCode(account, userid, nick, params, type = 'alipay') {
    let str = `userId=${userid}&nick=${nick}&type=${type}&${formatObjectToString(params)}`
    let url = `https://dj.palmestore.com/zycl/gold/withdrawSubmitNoCheckCode?${account.uri}&${str}&signType=1`

    let opt = populateUrlObject(url)
    let res = await get(opt)
    if (res.code === 0) {
        console.log(toJSON(res))
        if (res.body.orderNo != undefined) {
            console.log(`【${account.nickname}】${res.body.desc??=''} ${res.body.desc1??=''}${type==='alipay'?'支付宝':'微信'} ${params.money} 元提现成功`)
        }
    } else {
        console.log(`【${account.nickname}】${type==='alipay'?'支付宝':'微信'}提现失败，${toJSON(res)}，url=${url}`)
    }
}

async function moneyChallenge(account) {
    let url = `https://dj.palmestore.com/zycl/api/gold/moneyChallenge?${account.uri}&ecpmMix=0.0&ecpmVideo=22.62&mcTacid=12385`
    let opt = populateUrlObject(url, account.cookie)
    let res = await get(opt);
    if (res.code === 0) {
        if (res.body.receiveRedPacketNum != res.body.redPacketMaxNum) {
            console.log(`【${account.nickname}】挑战任务观看次数${res.body.receiveRedPacketNum}次`)
            for (let i = 0; i < 10; i++) {
                console.log(`【${account.nickname}】开始第${i+1}次挑战任务观看`)
                let r = await moneyChallengeReport(account);
                r = await moneyChallengeReport(account, 2);
                if (!r) {
                    break
                }
                await $.wait(30000)
            }
        }

    } else {
        console.log(`【${account.nickname}】获取任务挑战信息失败，${toJSON(res)}`)
    }
}
async function moneyChallengeReport(account, type = 1) {
    let ts = getTimestamp();
    let signstr = `ecpm=24.64&timestamp=${ts}&type=${type}&usr=${account.usr}`
    let sign = createSign(signstr)
    let url = `https://dj.palmestore.com/zycl/api/gold/moneyChallengeReport?${signstr}&${account.uri}&sign=${sign}`
    let opt = populateUrlObject(url, account.cookie)
    let res = await get(opt);
    if (res.code === 0) {
        console.log(`【${account.nickname}】挑战任务${type === 1?'视频':'资讯'}观看成功！获得${res?.conf?.receiveMoney}金币`)
        return true
    } else if (res.code === 19022) {
        return false
    } else {
        console.log(`【${account.nickname}】挑战任务${type === 1?'视频':'资讯'}观看失败！${toJSON(res)}`)
        return false
    }
}

async function indexRefreshV5(account) {
    let u = `https://dj.palmestore.com/zycl/gold/indexRefreshV5?${account.uri}`
    let opt = populateUrlObject(u, account.cookie)
    if (account.os === 'ios') {
        opt = iosPopulateUrlObject(u, account.cookie)
    }
    let res = await get(opt)
    if (res.code === 0) {
        console.log(`账号状态：${res.body.isBlack ?'锁定':'未锁定'}`)
        //11039 VIDEOSIGNNEW 11042 VIDEO_POP_WINDOW 199,  10182 VIDEO_POP_WINDOW 39,   10047 VIDEOSIGNNEW 69
        if (res.body.video.globalPop.useStatus.remainingCount > 0) {
            // await videoV5(account,res.body.video.globalPop.info.id,res.body.video.globalPop.position)
            // await videoV5(account, 11042, 'VIDEO_POP_WINDOW')
        }
        await $.wait(5000)
        if (res.body.video.sign.useStatus.remainingCount > 0) {
            await videoV5(account, res.body.video.sign.info.id, res.body.video.sign.position)
            //
        }
    }
}

function runJavaScript(code) {
    const vm = require('vm');
    const context = vm.createContext({});
    // 在上下文中运行 JavaScript 代码
    vm.runInContext(code, context);
    return context;
}

function urlsign(ts, usr) {
    return createSign(`timestamp=${ts}&usr=${usr}`)
}

async function getAccount(account) {
    let ts = new Date().getTime()
    if (account.os === 'ios') {
        ts = iosTS()
    }
    let sign = urlsign(ts, account.usr)
    let u = `https://dj.palmestore.com/zyuc/api/user/accountInfo?sign=${sign}&timestamp=${ts}&${account.uri}`
    let opt = {
        url: u,
        headers: {
            'Host': "dj.palmestore.com",
            "user-agent": 'okhttp/3.11.0',
            "Accept-Encoding": "gzip",
        },
    }
    if (account.os === 'ios') {
        opt.headers['user-agent'] = 'Mozilla/5.0 (iPhone; CPU iPhone OS 15_0_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148'
    }
    let res = await get(opt)
    if (res.code === 0) {
        account.nickname = res.body.userInfo.userNick
        account.time = res.body.read.time
        console.log(`用户：${res.body.userInfo.userNick}`)
        console.log(`手机：${res.body.userInfo.phone}`)
        console.log(`已阅读${res.body.read.time}分钟`)
        console.log(`金币：${res.body.gold.goldAmount}，${res.body.gold.goldText}`)
        
    } else {
        console.log(`获取用户信息失败 ${toJSON(res)}`)
    }

}

async function readTimer(account, bookid = '12862854') {
    let ts = new Date().getTime()
    let bid = bookid
    let t = random(60, 70)
    let timer = t
    console.log(`【${account.nickname}】开始阅读 ${t} 秒`)
    let u = `https://dj.palmestore.com/dj_reading/out/readingTime/report?${account.uri}`
    let r1 = {
        "bid": bid,
        "format": "zyepub",
        "time": timer,
        "resType": "read"
    }
    let date = getDate()
    let d = {}
    d[date] = {
        "d1": [r1]
    }
    let reqBody = {
        "alias": "9888eb1254c84e0b8d9356bc36d3183e",
        "data": JSON.stringify([{
            "type": 6,
            "data": d
        }]),
        "platformId": "501656",
        "sign": '',
        "timestamp": ts + "",
        "user_name": account.usr,
    }
    let signdata = objToStr(reqBody)
    reqBody.sign = createSign(signdata)

    let bodybuffer = await gzip(JSON.stringify(reqBody))

    let opt = {
        url: u,
        headers: readHD,
        body: bodybuffer
    }
    let res = await post(opt)
    if (res.code === 0) {
        console.log(`【${account.nickname}】阅读成功`)
    } else {
        console.log(`【${account.nickname}】阅读失败 ${toJSON(res)}`)
    }
}

function getUri(url) {
    let arr = [];
    let uri = url
    uri = uri.split('&');
    uri.filter(item => {
        if (item.indexOf('sign') > -1 || item.indexOf('timestamp') > -1) {
            return false;
        } else {
            arr.push(item);
        }
    })
    return arr.join('&');
}

function formatTime(time) {
    let date = new Date(time)
    let year = date.getFullYear()
    let month = date.getMonth() + 1
    let day = date.getDate()
    let hour = date.getHours()
    let minute = date.getMinutes()
    let second = date.getSeconds()
    return `${year}-${month}-${day} ${hour}:${minute}:${second}`
}

function htmlParse(html) {
    return cheerio.load(html)
}

function getQueryVariable(url, variable) {
    var vars = url.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] === variable) {
            return pair[1];
        }
    }
    return '';
}

function getDate() {
    let date = new Date()
    let year = date.getFullYear()
    let month = date.getMonth() + 1
    month = month < 10 ? '0' + month : month
    let day = date.getDate()
    day = day < 10 ? '0' + day : day
    return `${year}-${month}-${day}`
}

function createSign(data) {
    const sign = crypto.createSign('RSA-SHA1');
    sign.update(sortUri(data));
    sign.end();
    return sign.sign(privateKey).toString('base64')

}
async function gzip(data) {
    return new Promise((resolve, reject) => {
        zlib.gzip(data, function (err, buffer) {
            if (err) {
                reject(err)
            } else {
                resolve(buffer)
            }
        })
    })
}
async function msg(title, text) {
    if ($.isNode()) {
        await notify.sendNotify(title, text)
    } else {
        $.msg(title, '', text)
    }
}

function random(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}
//遍历对象属性，返回数组
function objToStr(obj) {
    let arr = []
    for (let i in obj) {
        if (i === 'sign') {
            continue
        }
        arr.push(`${i}=${obj[i]}`)
    }
    arr = arr.sort()
    return arr.join('&')
}

function toJSON(obj) {
    if (typeof obj === 'string') {
        return obj
    } else {
        try {
            return JSON.stringify(obj)
        } catch (e) {
            console.log(`对象转JSON出错 ${e}`)
        }
    }
}

function toObj(str) {
    if (typeof str === 'object') {
        return str
    } else {
        try {
            return JSON.parse(str)
        } catch (e) {
            console.log(`JSON转对象出错 ${str}------ ${e}`)
        }
    }
}

function sortUri(uri) {
    let arr = uri.split('&')
    arr.sort()
    return arr.join('&')
}

function iosReadTS() {
    let ts = Math.round(new Date().getTime() / 1000)
    let radom = random(345, 8923)
    return `${ts}.${radom}`
}

function iosTS() {
    let ts = new Date().getTime()
    let radom = random(345, 999)
    return `${ts}.${radom}`
}

function getTimestamp() {
    let ts = new Date().getTime()
    return ts
}
async function post(opt) {
    return new Promise((resolve, reject) => {
        $.post(opt, async (err, resp, data) => {
            try {
                if (err) {
                    console.log('post请求错误')
                    console.log($.toStr(err))
                } else {
                    if (debuglog) {

                        console.log(data)
                    }
                    httpResult = $.toObj(data)
                    resolve(httpResult)
                }
            } catch (e) {
                console.log(e)
            } finally {
                resolve()
            }
        })
    })
}

async function get(opt) {
    return new Promise((resolve, reject) => {
        $.get(opt, async (err, resp, data) => {
            try {
                if (err) {
                    console.log('get请求错误')
                    console.log($.toStr(err))
                } else {
                    if (debuglog) {
                        console.log(data)
                    }
                    // httpResult = $.toObj(data)
                    try {
                        resolve(JSON.parse(data))
                    } catch (e) {
                        // console.log(`JSON转对象出错 数据：${data}`)
                        resolve(data)
                    }
                }
            } catch (e) {
                console.log(e)
            } finally {
                resolve()
            }
        })
    })
}

function populateUrlObject(url, cookie = '', body = '') {
    let host = (url.split('//')[1]).split('/')[0]
    let urlObject = {
        url: url,
        headers: {
            'Host': "dj.palmestore.com",
            "user-agent": 'okhttp/3.11.0',
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
            "x-requested-with": "XMLHttpRequest",
            "Accept-Encoding": "gzip, deflate",
        },
    }
    if (body) urlObject.body = body
    if (cookie) urlObject.headers['Cookie'] = cookie
    return urlObject;
}

function iosPopulateUrlObject(url, cookie = '', body = '') {
    let host = (url.split('//')[1]).split('/')[0]
    let urlObject = {
        url: url,
        headers: {
            'Host': "dj.palmestore.com",
            "user-agent": 'Mozilla/5.0 (iPhone; CPU iPhone OS 15_0_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148',
            "x-requested-with": "XMLHttpRequest",
            "Accept-Encoding": "gzip, deflate",
        },
    }
    if (body) urlObject.body = body
    if (cookie) urlObject.headers['Cookie'] = cookie
    return urlObject;
}

function iosFormPopulateUrlObject(url, cookie = '', body = '') {
    let host = (url.split('//')[1]).split('/')[0]
    let urlObject = {
        url: url,
        headers: {
            'Host': "dj.palmestore.com",
            "user-agent": 'Mozilla/5.0 (iPhone; CPU iPhone OS 15_0_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148',
            "x-requested-with": "XMLHttpRequest",
            "Accept-Encoding": "gzip, deflate",
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        },
    }
    if (body) urlObject.body = body
    if (cookie) urlObject.headers['Cookie'] = cookie
    return urlObject;
}

function Env(t, e) {
    class s {
        constructor(t) {
            this.env = t
        }
        send(t, e = "GET") {
            t = "string" == typeof t ? {
                url: t
            } : t;
            let s = this.get;
            return "POST" === e && (s = this.post), new Promise((e, i) => {
                s.call(this, t, (t, s, r) => {
                    t ? i(t) : e(s)
                })
            })
        }
        get(t) {
            return this.send.call(this.env, t)
        }
        post(t) {
            return this.send.call(this.env, t, "POST")
        }
    }
    return new class {
        constructor(t, e) {
            this.name = t, this.http = new s(this), this.data = null, this.dataFile = "box.dat", this.logs = [], this.isMute = !1, this.isNeedRewrite = !1, this.logSeparator = "\n", this.startTime = (new Date).getTime(), Object.assign(this, e), this.log("", `\ud83d\udd14${this.name}, \u5f00\u59cb!`)
        }
        isNode() {
            return "undefined" != typeof module && !!module.exports
        }
        isQuanX() {
            return "undefined" != typeof $task
        }
        isSurge() {
            return "undefined" != typeof $httpClient && "undefined" == typeof $loon
        }
        isLoon() {
            return "undefined" != typeof $loon
        }
        toObj(t, e = null) {
            try {
                return JSON.parse(t)
            } catch {
                return e
            }
        }
        toStr(t, e = null) {
            try {
                return JSON.stringify(t)
            } catch {
                return e
            }
        }
        getjson(t, e) {
            let s = e;
            const i = this.getdata(t);
            if (i) try {
                s = JSON.parse(this.getdata(t))
            } catch {}
            return s
        }
        setjson(t, e) {
            try {
                return this.setdata(JSON.stringify(t), e)
            } catch {
                return !1
            }
        }
        getScript(t) {
            return new Promise(e => {
                this.get({
                    url: t
                }, (t, s, i) => e(i))
            })
        }
        runScript(t, e) {
            return new Promise(s => {
                let i = this.getdata("@chavy_boxjs_userCfgs.httpapi");
                i = i ? i.replace(/\n/g, "").trim() : i;
                let r = this.getdata("@chavy_boxjs_userCfgs.httpapi_timeout");
                r = r ? 1 * r : 20, r = e && e.timeout ? e.timeout : r;
                const [o, h] = i.split("@"), a = {
                    url: `http://${h}/v1/scripting/evaluate`,
                    body: {
                        script_text: t,
                        mock_type: "cron",
                        timeout: r
                    },
                    headers: {
                        "X-Key": o,
                        Accept: "*/*"
                    }
                };
                this.post(a, (t, e, i) => s(i))
            }).catch(t => this.logErr(t))
        }
        loaddata() {
            if (!this.isNode()) return {}; {
                this.fs = this.fs ? this.fs : require("fs"), this.path = this.path ? this.path : require("path");
                const t = this.path.resolve(this.dataFile),
                    e = this.path.resolve(process.cwd(), this.dataFile),
                    s = this.fs.existsSync(t),
                    i = !s && this.fs.existsSync(e);
                if (!s && !i) return {}; {
                    const i = s ? t : e;
                    try {
                        return JSON.parse(this.fs.readFileSync(i))
                    } catch (t) {
                        return {}
                    }
                }
            }
        }
        writedata() {
            if (this.isNode()) {
                this.fs = this.fs ? this.fs : require("fs"), this.path = this.path ? this.path : require("path");
                const t = this.path.resolve(this.dataFile),
                    e = this.path.resolve(process.cwd(), this.dataFile),
                    s = this.fs.existsSync(t),
                    i = !s && this.fs.existsSync(e),
                    r = JSON.stringify(this.data);
                s ? this.fs.writeFileSync(t, r) : i ? this.fs.writeFileSync(e, r) : this.fs.writeFileSync(t, r)
            }
        }
        lodash_get(t, e, s) {
            const i = e.replace(/\[(\d+)\]/g, ".$1").split(".");
            let r = t;
            for (const t of i)
                if (r = Object(r)[t], void 0 === r) return s;
            return r
        }
        lodash_set(t, e, s) {
            return Object(t) !== t ? t : (Array.isArray(e) || (e = e.toString().match(/[^.[\]]+/g) || []), e.slice(0, -1).reduce((t, s, i) => Object(t[s]) === t[s] ? t[s] : t[s] = Math.abs(e[i + 1]) >> 0 == +e[i + 1] ? [] : {}, t)[e[e.length - 1]] = s, t)
        }
        getdata(t) {
            let e = this.getval(t);
            if (/^@/.test(t)) {
                const [, s, i] = /^@(.*?)\.(.*?)$/.exec(t), r = s ? this.getval(s) : "";
                if (r) try {
                    const t = JSON.parse(r);
                    e = t ? this.lodash_get(t, i, "") : e
                } catch (t) {
                    e = ""
                }
            }
            return e
        }
        setdata(t, e) {
            let s = !1;
            if (/^@/.test(e)) {
                const [, i, r] = /^@(.*?)\.(.*?)$/.exec(e), o = this.getval(i), h = i ? "null" === o ? null : o || "{}" : "{}";
                try {
                    const e = JSON.parse(h);
                    this.lodash_set(e, r, t), s = this.setval(JSON.stringify(e), i)
                } catch (e) {
                    const o = {};
                    this.lodash_set(o, r, t), s = this.setval(JSON.stringify(o), i)
                }
            } else s = this.setval(t, e);
            return s
        }
        getval(t) {
            return this.isSurge() || this.isLoon() ? $persistentStore.read(t) : this.isQuanX() ? $prefs.valueForKey(t) : this.isNode() ? (this.data = this.loaddata(), this.data[t]) : this.data && this.data[t] || null
        }
        setval(t, e) {
            return this.isSurge() || this.isLoon() ? $persistentStore.write(t, e) : this.isQuanX() ? $prefs.setValueForKey(t, e) : this.isNode() ? (this.data = this.loaddata(), this.data[e] = t, this.writedata(), !0) : this.data && this.data[e] || null
        }
        initGotEnv(t) {
            this.got = this.got ? this.got : require("got"), this.cktough = this.cktough ? this.cktough : require("tough-cookie"), this.ckjar = this.ckjar ? this.ckjar : new this.cktough.CookieJar, t && (t.headers = t.headers ? t.headers : {}, void 0 === t.headers.Cookie && void 0 === t.cookieJar && (t.cookieJar = this.ckjar))
        }
        get(t, e = (() => {})) {
            t.headers && (delete t.headers["Content-Type"], delete t.headers["Content-Length"]), this.isSurge() || this.isLoon() ? (this.isSurge() && this.isNeedRewrite && (t.headers = t.headers || {}, Object.assign(t.headers, {
                "X-Surge-Skip-Scripting": !1
            })), $httpClient.get(t, (t, s, i) => {
                !t && s && (s.body = i, s.statusCode = s.status), e(t, s, i)
            })) : this.isQuanX() ? (this.isNeedRewrite && (t.opts = t.opts || {}, Object.assign(t.opts, {
                hints: !1
            })), $task.fetch(t).then(t => {
                const {
                    statusCode: s,
                    statusCode: i,
                    headers: r,
                    body: o
                } = t;
                e(null, {
                    status: s,
                    statusCode: i,
                    headers: r,
                    body: o
                }, o)
            }, t => e(t))) : this.isNode() && (this.initGotEnv(t), this.got(t).on("redirect", (t, e) => {
                try {
                    if (t.headers["set-cookie"]) {
                        const s = t.headers["set-cookie"].map(this.cktough.Cookie.parse).toString();
                        this.ckjar.setCookieSync(s, null), e.cookieJar = this.ckjar
                    }
                } catch (t) {
                    this.logErr(t)
                }
            }).then(t => {
                const {
                    statusCode: s,
                    statusCode: i,
                    headers: r,
                    body: o
                } = t;
                e(null, {
                    status: s,
                    statusCode: i,
                    headers: r,
                    body: o
                }, o)
            }, t => {
                const {
                    message: s,
                    response: i
                } = t;
                e(s, i, i && i.body)
            }))
        }
        post(t, e = (() => {})) {
            if (t.body && t.headers && !t.headers["Content-Type"] && (t.headers["Content-Type"] = "application/x-www-form-urlencoded"), t.headers && delete t.headers["Content-Length"], this.isSurge() || this.isLoon()) this.isSurge() && this.isNeedRewrite && (t.headers = t.headers || {}, Object.assign(t.headers, {
                "X-Surge-Skip-Scripting": !1
            })), $httpClient.post(t, (t, s, i) => {
                !t && s && (s.body = i, s.statusCode = s.status), e(t, s, i)
            });
            else if (this.isQuanX()) t.method = "POST", this.isNeedRewrite && (t.opts = t.opts || {}, Object.assign(t.opts, {
                hints: !1
            })), $task.fetch(t).then(t => {
                const {
                    statusCode: s,
                    statusCode: i,
                    headers: r,
                    body: o
                } = t;
                e(null, {
                    status: s,
                    statusCode: i,
                    headers: r,
                    body: o
                }, o)
            }, t => e(t));
            else if (this.isNode()) {
                this.initGotEnv(t);
                const {
                    url: s,
                    ...i
                } = t;
                this.got.post(s, i).then(t => {
                    const {
                        statusCode: s,
                        statusCode: i,
                        headers: r,
                        body: o
                    } = t;
                    e(null, {
                        status: s,
                        statusCode: i,
                        headers: r,
                        body: o
                    }, o)
                }, t => {
                    const {
                        message: s,
                        response: i
                    } = t;
                    e(s, i, i && i.body)
                })
            }
        }
        time(t) {
            let e = {
                "M+": (new Date).getMonth() + 1,
                "d+": (new Date).getDate(),
                "H+": (new Date).getHours(),
                "m+": (new Date).getMinutes(),
                "s+": (new Date).getSeconds(),
                "q+": Math.floor(((new Date).getMonth() + 3) / 3),
                S: (new Date).getMilliseconds()
            };
            /(y+)/.test(t) && (t = t.replace(RegExp.$1, ((new Date).getFullYear() + "").substr(4 - RegExp.$1.length)));
            for (let s in e) new RegExp("(" + s + ")").test(t) && (t = t.replace(RegExp.$1, 1 == RegExp.$1.length ? e[s] : ("00" + e[s]).substr(("" + e[s]).length)));
            return t
        }
        msg(e = t, s = "", i = "", r) {
            const o = t => {
                if (!t) return t;
                if ("string" == typeof t) return this.isLoon() ? t : this.isQuanX() ? {
                    "open-url": t
                } : this.isSurge() ? {
                    url: t
                } : void 0;
                if ("object" == typeof t) {
                    if (this.isLoon()) {
                        let e = t.openUrl || t.url || t["open-url"],
                            s = t.mediaUrl || t["media-url"];
                        return {
                            openUrl: e,
                            mediaUrl: s
                        }
                    }
                    if (this.isQuanX()) {
                        let e = t["open-url"] || t.url || t.openUrl,
                            s = t["media-url"] || t.mediaUrl;
                        return {
                            "open-url": e,
                            "media-url": s
                        }
                    }
                    if (this.isSurge()) {
                        let e = t.url || t.openUrl || t["open-url"];
                        return {
                            url: e
                        }
                    }
                }
            };
            this.isMute || (this.isSurge() || this.isLoon() ? $notification.post(e, s, i, o(r)) : this.isQuanX() && $notify(e, s, i, o(r)));
            let h = ["", "==============\ud83d\udce3\u7cfb\u7edf\u901a\u77e5\ud83d\udce3=============="];
            h.push(e), s && h.push(s), i && h.push(i), console.log(h.join("\n")), this.logs = this.logs.concat(h)
        }
        log(...t) {
            t.length > 0 && (this.logs = [...this.logs, ...t]), console.log(t.join(this.logSeparator))
        }
        logErr(t, e) {
            const s = !this.isSurge() && !this.isQuanX() && !this.isLoon();
            s ? this.log("", `\u2757\ufe0f${this.name}, \u9519\u8bef!`, t.stack) : this.log("", `\u2757\ufe0f${this.name}, \u9519\u8bef!`, t)
        }
        wait(t) {
            return new Promise(e => setTimeout(e, t))
        }
        done(t = {}) {
            const e = (new Date).getTime(),
                s = (e - this.startTime) / 1e3;
            this.log("", `\ud83d\udd14${this.name}, \u7ed3\u675f! \ud83d\udd5b ${s} \u79d2`), this.log(), (this.isSurge() || this.isQuanX() || this.isLoon()) && $done(t)
        }
    }(t, e)
}