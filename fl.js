/*

  返利app
  cron:20 7-20 * * *

 */

const $ = new Env("返利app");
let flapp = $.isNode() ? (process.env.flapp ? process.env.flapp : "") : ($.getdata('flapp') ? $.getdata('flapp') : "")
const notify = $.isNode() ? require('./sendNotify') : '';
// const datefns = require('date-fns')

let debug = 0
let message = ''
class Account {
    constructor(url,index) {
        this.url = url
        this.cookie = `prouserid=${getValue(url,'u_id')}; PHPSESSID=${getValue(url,'verify_code')};`
        this.index = index
    }

    async  flushCookie() {
        let url = `https://fun.fanli.com/api/user/renew?${this.url}`
        let opt = buildURLObj(url)
        let res = await get(opt)
        if (res.status){
            console.log('cookie刷新成功！')
        }else {
            console.log(`cookie刷新失败，${toJSON(res)}`)
        }
    }

    async  taskList() {
        let url = `https://huodong.fanli.com/sign82580/ajaxMainInit`
        let opt = buildURLObj(url,'',this.cookie)
        let res = await get(opt)
        if (res.status){
            console.log(`账号【${this.index}】金币：${res.data.points} 金额：${res.data.money}`)
            message += `\n账号【${this.index}】金币：${res.data.points} 金额：${res.data.money}\n`
            let current = res.data.sign_info.current
            let day = res.data.sign_info.list.find(e => e.day === current)
            if (day){
                if (day.state){
                    console.log( `账号【${this.index}】已签到`)
                }else {
                    await this.signin(this.cookie)
                }
            }
            let tlist = res.data.task_list
            for (let t of tlist.new_user_task) {

                if (t.is_finish ){
                    console.log(`账号【${this.index}】[${t.title}]任务已完成`)
                }else {
                    await this.submitTask(this.cookie,t.id)
                }
            }
            for (let t of tlist.daily_task) {
                if (t.id !==  17){
                    if (t.is_finish ){
                        console.log(`账号【${this.index}】[${t.title}]任务已完成`)
                    }else {
                        await this.submitTask(this.cookie,t.id)
                    }
                }else {
                    const now = new Date();
                    const hour = now.getHours();
                    if (hour === 7 || hour === 12 || hour === 18)
                        await this.submitTask(this.cookie,t.id)

                }
            }
            if (res.data.video_task.is_finish === 0){
                for (let i = 1; i < res.data.video_task.video_pos.length; i++) {
                    await this.submitTask(this.cookie,res.data.video_task.id,i)
                    await $.wait(16000)
                }
            }

        }else {
            console.log(`账号【${this.index}】错误，${toJSON(res)}`)
            if (toJSON(res).indexOf('未登录') > -1){
                message += `\n账号【${this.index}】cookie失效\n`
            }
        }
    }

    async  signin(cookie) {
        let url = `https://huodong.fanli.com/sign82580/ajaxSetUserSign`
        let opt = buildURLObj(url,'',cookie)
        let res = await get(opt)
        if (res.status){
            console.log(`账号【${this.index}】签到成功，获得${res.data.reward}金币✔`)
        }else {
            console.log(`账号【${this.index}】签到错误❌，${toJSON(res)}`)
        }
    }

    async  submitTask(cookie,id,pos = '') {
        let url = `https://huodong.fanli.com/sign82580/ajaxSetUserTask?id=${id}&pos=${pos}`
        let opt = buildURLObj(url,'',cookie)
        let res = await get(opt)
        if (res.status){
            console.log(`账号【${this.index}】${id}-${pos}任务完成✔`)
        }else {
            console.log(`账号【${this.index}】${id}-${pos}任务提交失败❌，${res.info}`)
        }
    }

}
!(async () => {
    if (flapp === ''){
        console.log(`没有url`)
    }else {
        checkEnvs()
        let accountList = []
        for (let i = 0; i < flapp.length; i++) {

            accountList.push(new Account(flapp[i],i+1))
        }
        for (let fl of accountList) {
            await fl.flushCookie()
            await fl.taskList()
            console.log('-------------------------------')
        }

        await msg('返利app',message)
    }


})()
    .catch((e) => $.logErr(e))
    .finally(() => $.done())

function getCookieValue(str,key) {
    const values = {};
    str.split(';').forEach(pair => {
        const parts = pair.split('=');
        const key = parts[0].trim();
        const value = parts[1].trim();
        values[key] = value;
    });
    return values[key]
}
function getValue(str,key) {
    let reg = new RegExp(`${key}=([^&]*)`)
    let res = reg.exec(str)
    return res[1]
}

// function formatTime(timestamp = `${Date.now()}`,formatstr = 'yyyy MM dd HH:mm:ss') {
//     timestamp = String(timestamp).padEnd(13, '0')
//
//     return datefns.format(Number(timestamp),formatstr,{locale:zh})
// }

function checkEnvs() {
    if (flapp.indexOf('@') > -1){
        flapp = flapp.split('@')
    }else if (flapp.indexOf('\n') > -1){
        flapp = flapp.split('\n')
    }else {
        flapp = [flapp]
    }
}

function toJSON(obj) {
    if (typeof obj === 'string'){
        return obj
    }else {
        try {
            return JSON.stringify(obj)
        }catch (e) {
            console.log(`对象转JSON出错 ${e}`)
        }
    }
}
function toObj(str) {
    if (typeof str === 'object'){
        return str
    }else {
        try {
            return JSON.parse(str)
        }catch (e) {
            console.log(`JSON转对象出错 ${str}------ ${e}`)
        }
    }
}
async function msg(title, text) {
    if ($.isNode()){
        await notify.sendNotify(title, text)
    }else {
        $.msg(title,'',text)
    }

}
function getTimestamp() {
    let ts = Date.now()
    return ts
}
function buildURLObj(url,body='',cookie){
    let host = (url.split('//')[1]).split('/')[0]
    let urlObject = {
        url: url,
        headers: {
            'user-agent':'Mozilla/5.0 (Linux; Android 10; PBEM00 Build/QKQ1.190918.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/112.0.5615.135 Mobile Safari/537.36 Fanli/7.19.55.4 (ID:2-611468462-66232629764997-56-0; WVC:WV; SCR:1080*2340-3.0)',
            'X-Requested-With':'XMLHttpRequest',
            'cookie':cookie
        },
    }
    if(body) urlObject.body = body
    return urlObject;
}
async function post(opt) {
    return new Promise((resolve, reject) => {
        $.post(opt,async(err,resp,data) => {
            try {
                if (err){
                    console.log('post请求错误')
                    console.log($.toStr(err))
                }else {
                    if (debug){
                        console.log(`--------------------post请求-------------------`)
                        console.log(toJSON(opt))
                        console.log('--------------------post响应-------------------')
                        console.log(data)}
                    try {
                        resolve(toObj(data))
                    }catch (e) {
                        console.log(`JSON转对象出错 数据：${data}`)
                        resolve(data)
                    }
                }
            }catch (e) {
                console.log(e)
            }finally{
                resolve()
            }
        })
    })
}

async function get(opt) {
    return new Promise((resolve, reject) => {
        $.get(opt,async(err,resp,data) => {
            try {
                if (err){
                    console.log('get请求错误')
                    console.log($.toStr(err))
                }else {
                    if (debug){
                        console.log(`--------------------get请求-------------------`)
                        console.log(toJSON(opt))
                        console.log('--------------------get响应-------------------')
                        console.log(data)}
                    try {
                        resolve(toObj(data))
                    }catch (e) {
                        console.log(`JSON转对象出错 数据：${data}`)
                        resolve(data)
                    }
                }
            }catch (e) {
                console.log(e)
            }finally{
                resolve()
            }
        })
    })
}

function Env(t, e) { class s { constructor(t) { this.env = t } send(t, e = "GET") { t = "string" == typeof t ? { url: t } : t; let s = this.get; return "POST" === e && (s = this.post), new Promise((e, i) => { s.call(this, t, (t, s, r) => { t ? i(t) : e(s) }) }) } get(t) { return this.send.call(this.env, t) } post(t) { return this.send.call(this.env, t, "POST") } } return new class { constructor(t, e) { this.name = t, this.http = new s(this), this.data = null, this.dataFile = "box.dat", this.logs = [], this.isMute = !1, this.isNeedRewrite = !1, this.logSeparator = "\n", this.startTime = (new Date).getTime(), Object.assign(this, e), this.log("", `\ud83d\udd14${this.name}, \u5f00\u59cb!`) } isNode() { return "undefined" != typeof module && !!module.exports } isQuanX() { return "undefined" != typeof $task } isSurge() { return "undefined" != typeof $httpClient && "undefined" == typeof $loon } isLoon() { return "undefined" != typeof $loon } toObj(t, e = null) { try { return JSON.parse(t) } catch { return e } } toStr(t, e = null) { try { return JSON.stringify(t) } catch { return e } } getjson(t, e) { let s = e; const i = this.getdata(t); if (i) try { s = JSON.parse(this.getdata(t)) } catch { } return s } setjson(t, e) { try { return this.setdata(JSON.stringify(t), e) } catch { return !1 } } getScript(t) { return new Promise(e => { this.get({ url: t }, (t, s, i) => e(i)) }) } runScript(t, e) { return new Promise(s => { let i = this.getdata("@chavy_boxjs_userCfgs.httpapi"); i = i ? i.replace(/\n/g, "").trim() : i; let r = this.getdata("@chavy_boxjs_userCfgs.httpapi_timeout"); r = r ? 1 * r : 20, r = e && e.timeout ? e.timeout : r; const [o, h] = i.split("@"), a = { url: `http://${h}/v1/scripting/evaluate`, body: { script_text: t, mock_type: "cron", timeout: r }, headers: { "X-Key": o, Accept: "*/*" } }; this.post(a, (t, e, i) => s(i)) }).catch(t => this.logErr(t)) } loaddata() { if (!this.isNode()) return {}; { this.fs = this.fs ? this.fs : require("fs"), this.path = this.path ? this.path : require("path"); const t = this.path.resolve(this.dataFile), e = this.path.resolve(process.cwd(), this.dataFile), s = this.fs.existsSync(t), i = !s && this.fs.existsSync(e); if (!s && !i) return {}; { const i = s ? t : e; try { return JSON.parse(this.fs.readFileSync(i)) } catch (t) { return {} } } } } writedata() { if (this.isNode()) { this.fs = this.fs ? this.fs : require("fs"), this.path = this.path ? this.path : require("path"); const t = this.path.resolve(this.dataFile), e = this.path.resolve(process.cwd(), this.dataFile), s = this.fs.existsSync(t), i = !s && this.fs.existsSync(e), r = JSON.stringify(this.data); s ? this.fs.writeFileSync(t, r) : i ? this.fs.writeFileSync(e, r) : this.fs.writeFileSync(t, r) } } lodash_get(t, e, s) { const i = e.replace(/\[(\d+)\]/g, ".$1").split("."); let r = t; for (const t of i) if (r = Object(r)[t], void 0 === r) return s; return r } lodash_set(t, e, s) { return Object(t) !== t ? t : (Array.isArray(e) || (e = e.toString().match(/[^.[\]]+/g) || []), e.slice(0, -1).reduce((t, s, i) => Object(t[s]) === t[s] ? t[s] : t[s] = Math.abs(e[i + 1]) >> 0 == +e[i + 1] ? [] : {}, t)[e[e.length - 1]] = s, t) } getdata(t) { let e = this.getval(t); if (/^@/.test(t)) { const [, s, i] = /^@(.*?)\.(.*?)$/.exec(t), r = s ? this.getval(s) : ""; if (r) try { const t = JSON.parse(r); e = t ? this.lodash_get(t, i, "") : e } catch (t) { e = "" } } return e } setdata(t, e) { let s = !1; if (/^@/.test(e)) { const [, i, r] = /^@(.*?)\.(.*?)$/.exec(e), o = this.getval(i), h = i ? "null" === o ? null : o || "{}" : "{}"; try { const e = JSON.parse(h); this.lodash_set(e, r, t), s = this.setval(JSON.stringify(e), i) } catch (e) { const o = {}; this.lodash_set(o, r, t), s = this.setval(JSON.stringify(o), i) } } else s = this.setval(t, e); return s } getval(t) { return this.isSurge() || this.isLoon() ? $persistentStore.read(t) : this.isQuanX() ? $prefs.valueForKey(t) : this.isNode() ? (this.data = this.loaddata(), this.data[t]) : this.data && this.data[t] || null } setval(t, e) { return this.isSurge() || this.isLoon() ? $persistentStore.write(t, e) : this.isQuanX() ? $prefs.setValueForKey(t, e) : this.isNode() ? (this.data = this.loaddata(), this.data[e] = t, this.writedata(), !0) : this.data && this.data[e] || null } initGotEnv(t) { this.got = this.got ? this.got : require("got"), this.cktough = this.cktough ? this.cktough : require("tough-cookie"), this.ckjar = this.ckjar ? this.ckjar : new this.cktough.CookieJar, t && (t.headers = t.headers ? t.headers : {}, void 0 === t.headers.Cookie && void 0 === t.cookieJar && (t.cookieJar = this.ckjar)) } get(t, e = (() => { })) { t.headers && (delete t.headers["Content-Type"], delete t.headers["Content-Length"]), this.isSurge() || this.isLoon() ? (this.isSurge() && this.isNeedRewrite && (t.headers = t.headers || {}, Object.assign(t.headers, { "X-Surge-Skip-Scripting": !1 })), $httpClient.get(t, (t, s, i) => { !t && s && (s.body = i, s.statusCode = s.status), e(t, s, i) })) : this.isQuanX() ? (this.isNeedRewrite && (t.opts = t.opts || {}, Object.assign(t.opts, { hints: !1 })), $task.fetch(t).then(t => { const { statusCode: s, statusCode: i, headers: r, body: o } = t; e(null, { status: s, statusCode: i, headers: r, body: o }, o) }, t => e(t))) : this.isNode() && (this.initGotEnv(t), this.got(t).on("redirect", (t, e) => { try { if (t.headers["set-cookie"]) { const s = t.headers["set-cookie"].map(this.cktough.Cookie.parse).toString(); this.ckjar.setCookieSync(s, null), e.cookieJar = this.ckjar } } catch (t) { this.logErr(t) } }).then(t => { const { statusCode: s, statusCode: i, headers: r, body: o } = t; e(null, { status: s, statusCode: i, headers: r, body: o }, o) }, t => { const { message: s, response: i } = t; e(s, i, i && i.body) })) } post(t, e = (() => { })) { if (t.body && t.headers && !t.headers["Content-Type"] && (t.headers["Content-Type"] = "application/x-www-form-urlencoded"), t.headers && delete t.headers["Content-Length"], this.isSurge() || this.isLoon()) this.isSurge() && this.isNeedRewrite && (t.headers = t.headers || {}, Object.assign(t.headers, { "X-Surge-Skip-Scripting": !1 })), $httpClient.post(t, (t, s, i) => { !t && s && (s.body = i, s.statusCode = s.status), e(t, s, i) }); else if (this.isQuanX()) t.method = "POST", this.isNeedRewrite && (t.opts = t.opts || {}, Object.assign(t.opts, { hints: !1 })), $task.fetch(t).then(t => { const { statusCode: s, statusCode: i, headers: r, body: o } = t; e(null, { status: s, statusCode: i, headers: r, body: o }, o) }, t => e(t)); else if (this.isNode()) { this.initGotEnv(t); const { url: s, ...i } = t; this.got.post(s, i).then(t => { const { statusCode: s, statusCode: i, headers: r, body: o } = t; e(null, { status: s, statusCode: i, headers: r, body: o }, o) }, t => { const { message: s, response: i } = t; e(s, i, i && i.body) }) } } time(t) { let e = { "M+": (new Date).getMonth() + 1, "d+": (new Date).getDate(), "H+": (new Date).getHours(), "m+": (new Date).getMinutes(), "s+": (new Date).getSeconds(), "q+": Math.floor(((new Date).getMonth() + 3) / 3), S: (new Date).getMilliseconds() }; /(y+)/.test(t) && (t = t.replace(RegExp.$1, ((new Date).getFullYear() + "").substr(4 - RegExp.$1.length))); for (let s in e) new RegExp("(" + s + ")").test(t) && (t = t.replace(RegExp.$1, 1 == RegExp.$1.length ? e[s] : ("00" + e[s]).substr(("" + e[s]).length))); return t } msg(e = t, s = "", i = "", r) { const o = t => { if (!t) return t; if ("string" == typeof t) return this.isLoon() ? t : this.isQuanX() ? { "open-url": t } : this.isSurge() ? { url: t } : void 0; if ("object" == typeof t) { if (this.isLoon()) { let e = t.openUrl || t.url || t["open-url"], s = t.mediaUrl || t["media-url"]; return { openUrl: e, mediaUrl: s } } if (this.isQuanX()) { let e = t["open-url"] || t.url || t.openUrl, s = t["media-url"] || t.mediaUrl; return { "open-url": e, "media-url": s } } if (this.isSurge()) { let e = t.url || t.openUrl || t["open-url"]; return { url: e } } } }; this.isMute || (this.isSurge() || this.isLoon() ? $notification.post(e, s, i, o(r)) : this.isQuanX() && $notify(e, s, i, o(r))); let h = ["", "==============\ud83d\udce3\u7cfb\u7edf\u901a\u77e5\ud83d\udce3=============="]; h.push(e), s && h.push(s), i && h.push(i), console.log(h.join("\n")), this.logs = this.logs.concat(h) } log(...t) { t.length > 0 && (this.logs = [...this.logs, ...t]), console.log(t.join(this.logSeparator)) } logErr(t, e) { const s = !this.isSurge() && !this.isQuanX() && !this.isLoon(); s ? this.log("", `\u2757\ufe0f${this.name}, \u9519\u8bef!`, t.stack) : this.log("", `\u2757\ufe0f${this.name}, \u9519\u8bef!`, t) } wait(t) { return new Promise(e => setTimeout(e, t)) } done(t = {}) { const e = (new Date).getTime(), s = (e - this.startTime) / 1e3; this.log("", `\ud83d\udd14${this.name}, \u7ed3\u675f! \ud83d\udd5b ${s} \u79d2`), this.log(), (this.isSurge() || this.isQuanX() || this.isLoon()) && $done(t) } }(t, e) }
