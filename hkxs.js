/**
 * 浩看小说，得间同账号，
 *
 * cron:15 6-23/2 * * *
 * const $ = new Env('浩看小说')
 * id：25 金额：1 元 提现要求：连续签到天数3天 
 * id：26 金额：5 元 提现要求：连续签到天数10天 
 * id：30 金额：30 元 提现要求：连续签到天数25天 
 * id：27 金额：15 元 提现要求：连续签到天数0天 
 * id：28 金额：30 元 提现要求：连续签到天数0天 
 * id：99 金额：0.3 提现要求：连续签到天数0天 阅读5分钟
 * id：100 金额：1 提现要求：连续签到天数3天 
 * id：101 金额：5 提现要求：连续签到天数10天 
 * id：102 金额：15 提现要求：连续签到天数0天 
 */


const $ = new Env('浩看小说');
let hkxsapp = $.isNode() ? (process.env.hkxsapp ? process.env.hkxsapp : "") : ($.getdata('hkxsapp') ? $.getdata('hkxsapp') : "")
let hkxsapptx = $.isNode() ? (process.env.hkxsapptx ? process.env.hkxsapptx : false) : ($.getdata('hkxsapptx') ? $.getdata('hkxsapptx') : false)

const money = 30

const qs = require("qs");
const crypto = require("crypto");
const zlib = require("zlib");
const cheerio = require("cheerio");
const { URLSearchParams } = require('url');

const basehost = 'https://welfare-dj.palmestore.com'
const debuglog = false
let privateKey = '-----BEGIN PRIVATE KEY-----\n' +
    'MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAMXGjyS3p+3AVnlBJe5VQ6tC9inh8tVBve4r+yBjC5HQD6th2n3tSyuNVYaNRAFSEq+OENwnwwhjbYUnjLWb+qZscB43K1+4/WlKdvfgwQVXm0ZQ2+jMBf+165UBEEuuWT2WqXeKkkUqPQta5lrt4eFfbo53JcOO4D5fDSGQS5bZAgMBAAECgYAor4I/AXEQXeLsKtTMxMmY77uIPi0gZdfWqUGOFhIJOw4eKZEzGp++I+MWPPVieCnT55vcTmm2zg13uP0fVykmukWqZszG/ZNpPKYleOqnZOqQj7O3au8Ywz18F/pqD++PsUzxRVeXxSOOwmjQ0D2Pe/9yutz62pyiFGAzDsaI6QJBAMn8DeBT3AtcWuONdiHL3yC4NkGJDdyBbMOaWyvrcvUUZr13uS9mZO6pLTN6v9tkmPUdvYxcPTJ9wdGR7NcNPDsCQQD6qluGI2VAlz4s5UoDnelFKrwDPeiruE3I6wsrasK6h37DsAE6OrQgx2dm4yH7ntJHUlJCZ5ay1EBNfEexgQv7AkA1r2vUwxVKY7q4nqHWa8SbgrrRAmePw0qwVreC3erJHyoLk+XBpnqPQKIF+8tAueU5yTTXOLD/WZOJazrDEf5/AkBpwG+Ggu5Xtrcbd8ynA/sDHElf0MGVmNbwOgFnWs42pa1cX6fU6ilOXvIH3TFcF6A9SMS9kThpz9QlHJaek4P7AkAavQillA/wnrha9GsK5UFmzmwNfkjLLW4psAUsXOsqFXWMoxTd0xWuSbuVOzERpbFMBl1VoZQmD9BLSVOTNe+v' +
    '\n-----END PRIVATE KEY-----'

class Account {
    constructor(param,usr) {
        this.param = param
        this.nick = ''
        this.usr = usr
        this.signinvideo = false
        this.advideo = false
    }

}

!(async () => {
   
    hkxsapp = Envs()

    let accountArr = []
    if (hkxsapp.length > 0){
        for (let i = 0; i < hkxsapp.length; i++) {
            if(hkxsapp[i] === ''){continue}
            accountArr.push(new Account(hkxsapp[i],getValue(hkxsapp[i],'usr')))
        }
    

        console.log(`共${accountArr.length}个账号`)
        for (let i = 0; i < accountArr.length; i++) {
            console.log(`第${i+1}个账号`)
            await applaunch(accountArr[i],'time')
            await applaunch(accountArr[i],'oaid')
            await initconfig(accountArr[i])
            await msacert(accountArr[i])
            await accountInfo(accountArr[i])
            await userInfo(accountArr[i])
            await taskList(accountArr[i])
           
            //提现
            if(hkxsapptx){
                await payBindInfo(accountArr[i])
            }
            //现金余额
            // await withdrawMoney(accountArr[i])

            //现金流水
            await cashrecord(accountArr[i])
           
            // for (let j = 0; j < 10; j++) {
            //     await adWallGetCount(accountArr[i])
            //     await $.wait(5000)
            // }

            // await videoReport(accountArr[i],11106,'VIDEOSIGNNEW','普通签到')
            // await videoReport(accountArr[i],11105,'VIDEO_POP_WINDOW','宝箱广告')
            await videoReport(accountArr[i],11295,'VIDEO_POP_WINDOW','全局')
            console.log('-------------')
        }
        // for (let j = 0; j < 2; j++) {
        //     for (let i = 0; i < accountArr.length; i++) {
        //          await videoReport(accountArr[i],11295,'VIDEO_POP_WINDOW','看视频赚金币')
        //     }
        //     await $.wait(30000)
        // }
        // for (let j = 0; j < 2; j++) {
        //     for (let i = 0; i < accountArr.length; i++) {
        //         await videoReport(accountArr[i],11106,'VIDEOSIGNNEW','普通签到')
        //     }
        //     await $.wait(30000)
        // }
        accountArr = accountArr.filter(item => {return item.time < 600})
        
        //听书时长
        // for (let i = 0; i < 30; i++) {
        //     if(accountArr.length > 0){
        //         for (let i = 0; i < accountArr.length; i++) {
        //             readTime(accountArr[i],'TTS')
        //         }
        //         await $.wait(40000)
        //     }
            
        // }

        //小说时长
        for (let i = 0; i < 15; i++) {
            if(accountArr.length > 0){
                for (let i = 0; i < accountArr.length; i++) {
                    readTime(accountArr[i])
                }
                await $.wait(30000)
            }
        }

    }else {
        console.log(`环境变量hkxsapp没有设置`)
    }




    // let signuri = `timestamp=1691850283494&usr=j152103400&param=11106&p2=331104` //
    // let signdata = sortUri(signuri)
    // console.log(createSign(signdata))



})()
    .catch((e) => $.logErr(e))
    .finally(() => $.done())

function Envs() {
    let t = []
    if (hkxsapp.indexOf('#') > -1){
        t = hkxsapp.split('#')
    }else if (hkxsapp.indexOf('@') > -1){
        t = hkxsapp.split('@')
    }else if (hkxsapp.indexOf('\n') > -1){
        t = hkxsapp.split('\n')
    }else if (hkxsapp.length > 0) {
        t.push(hkxsapp)
    }
    return t


}
async function applaunch(account,type){
    let ts = getTimestamp()
    let signuri = `timestamp=${ts}&type=${type}&usr=${account.usr}` //
    let sign = createSign(sortQueryParams(signuri))
    
    let url = `https://dj.palmestore.com/zybk/api/powerup/launch?${account.param}&timestamp=${ts}&type=${type}&sign=${sign}`
    let opt = populateUrlObject(url)
    opt.headers['Host'] = 'dj.palmestore.com'
    let res = await get(opt);
    if (res.code === 0){
        console.log(`app启动成功`)
    }else {
        console.log(`app启动成功，`,toJSON(res))
    }
}
async function msacert(account){
   
    let url = `https://dj.palmestore.com/zybk/api/powerup/msacert?${account.param}`
    let opt = populateUrlObject(url)
    opt.headers['Host'] = 'dj.palmestore.com'
    let res = await get(opt);
    if (res.code === 0){
        console.log(`msacert成功`)
    }else {
        console.log(`msacert成功，`,toJSON(res))
    }
}
async function initconfig(account){
    let ts = getTimestamp()
    let signuri = `timestamp=${ts}&usr=${account.usr}` //
    let sign = createSign(sortQueryParams(signuri))
    
    let url = `https://dj.palmestore.com/zybk/api/powerup/initconfig?${account.param}&timestamp=${ts}&usr=${account.usr}&sign=${sign}`
    let opt = populateUrlObject(url)
    opt.headers['Host'] = 'dj.palmestore.com'
    let res = await get(opt);
    if (res.code === 0){
        console.log(`initconfig成功`)
    }else {
        console.log(`initconfig成功，`,toJSON(res))
    }
}
async function accountInfo(account) {
    let ts = getTimestamp()
    let signuri = `timestamp=${ts}&usr=${account.usr}` //
    let sign = createSign(sortQueryParams(signuri))
    
    let url = `https://dj.palmestore.com/zyuc/api/user/accountInfo?pluginVersion=752.4&sign=${sign}&${account.param}&timestamp=${ts}`
    let opt = populateUrlObject(url)
    opt.headers['Host'] = 'dj.palmestore.com'
    let res = await get(opt);
    if (res.code === 0){
        console.log(`账号：${res.body.userInfo.userNick}`)
        account.nick = res.body.userInfo.userNick
        console.log(`号码：${res.body.userInfo.phone}`)
        console.log(`今天阅读：${res.body.read.time} 分钟`)
        account.time = res.body.read.time
        // console.log(`金币：${res.body.gold.goldAmount}`)
    }else {
        console.log(`获取账号信息失败，`,toJSON(res))
    }
}

async function payBindInfo(account) {
    let opt = signHeader('/welfare/web/user/withdraw/schedule',account.param,populateUrlObject(''))
    let res = await get(opt)
    if (res.code === 0){
        console.log(`[${account.nick}] 连续登录天数 ${res.body.continue_login_days}`)
        console.log(`[${account.nick}] ${res.body.bind_info.wechat_name !== ''?`微信 ${res.body.bind_info.wechat_name} 已绑定`:'微信未绑定'}  ${res.body.bind_info.ali_name !== ''?`支付宝 ${res.body.bind_info.ali_name} 已绑定`:'支付宝未绑定'}`)
        console.log(`[${account.nick}] 今天阅读 ${res.body.read_info.reading.day} 分钟`)

        if (res.body.bind_info.wechat_name !== ''){
            await withdrawProducts(account,res.body.continue_login_days,'wechat')
        }else if (res.body.bind_info.ali_name !== ''){
            await withdrawProducts(account,res.body.continue_login_days,'alipay')
        }else {
            console.log(`[${account.nick}] 没有绑定提现方式`)
        }


    }else {
        console.log(`[${account.nick}] 获取支付绑定信息失败，`,toJSON(res))
    }
}


async function withdrawProducts(account,days,withdrawType) {
    let opt = signHeader('/welfare/web/withdraw/info',account.param,populateUrlObject(''))
    let res = await get(opt)
    if (res.code === 0){
        let plist = res.body.withdraw_products[0].list
        let prodect = plist[plist.length-1]
        // for (let prodect of plist) {
            console.log(`[${account.nick}] id：${prodect.id} 金额：${prodect.cash} 元 提现要求：连续签到天数${prodect.start_day>0?prodect.start_day:0}天 ${prodect.tag}`)
            //判断登录天数
            if (prodect.cash === money && days >= prodect.start_day){
                let useropt = signHeader('/welfare/web/user/info',account.param,populateUrlObject(''))
                let userres = await get(useropt)
                if (userres.code === 0){
                    console.log(`[${account.nick}] 金币：${userres.body.total_coin} 现金：${userres.body.total_cash}`)
                    //判断提现金额
                    if (userres.body.total_cash > prodect.cash){
                        await withdraw(account,prodect,withdrawType)
                    }else{
                        console.log(`[${account.nick}] 余额不足，跳过提现`)
                    }
                }else {
                    console.log(`[${account.nick}] 获取现金信息失败，`,toJSON(userres))
                }
            }
        // }
    }else {
        console.log(`[${account.nick}] 获取现金信息失败，`,toJSON(res))
    }
}
//alipay?
async function withdraw(account,product,method = 'wechat') {
    let tmp = `${account.param}&type=cash_wallet&coin=${product.coin}&price=${product.price}&product_id=94&item_id=${product.id}&method=${method}&sign=&reward_type=${product.reward_type}`

    let opt = signHeader('/welfare/web/withdraw/exec',tmp,populateUrlObject(''))
    let res = await get(opt)
    if (res.code === 0){
        console.log(`[${account.nick}] ${method==='wechat'?'微信':'支付宝'}提现 ${product.price} 元成功`)
    }else {
        console.log(`[${account.nick}] ${method==='wechat'?'微信':'支付宝'}提现失败，`,toJSON(res))
    }
}

async function withdrawMoney(account) {
    let opt = signHeader('/welfare/web/user/withdraw/statistics',account.param,populateUrlObject(''))
    let res = await get(opt)
    if (res.code === 0){
        console.log(`[${account.nick}] 已提现 ${res.body.extract_success_amount} 元，等待到账 ${res.body.extract_amount} 元`)
    }else {
        console.log(`[${account.nick}] 获取已提现信息失败，`,toJSON(res))
    }
}

async function cashrecord(account) {
    let opt = signHeader('/welfare/web/user/cash/record',account.param+'&page=1&size=20&source=mine&activeTab=2&showContentInStatusBar=1',populateUrlObject(''))
    let res = await get(opt)
    if (res.code === 0){
        console.log(`----${account.nick} 现金流水----`)
        let i = 0
        for (let c of res.body.list) {
            if(i>3){
                break
            }
            console.log(`${c.create_time} ${c.desc} ${c.amount}`)
            i++
        }
    }else {
        console.log(`[${account.nick}] 获取流水信息失败，`,toJSON(res))
    }
}



async function readTime(account,type = 'read') {
    let sec = random(60,70)
    let datestr = getDateString2()
    let datastr = `[{\"type\":6,\"data\":{\"${datestr}\":{\"d1\":[{\"bid\":\"12815167\",\"format\":\"zyepub\",\"time\":${sec},\"resType\":\"${type}\"}]}}}]`
    let body = {
        "data": datastr,
        "timestamp": `${getTimestamp()}`,
        "user_name": `${account.usr}`
    }
    let tmpbody = objToStr(body)


    let sign = createSign(sortAndFilterParams(account.param+'&'+tmpbody))
    body.sign = sign
    let bodybuffer = await gzip(JSON.stringify(body))
    let url = `https://api-dj.palmestore.com/reading/open/time/report?${account.param}`
    let opt = {url: url, headers: {
            'Host': "api-dj.palmestore.com",
            "user-agent":'Dalvik/2.1.0 (Linux; U; Android 10; PBEM00 Build/QKQ1.190918.001)',
            "Content-Type":"text/plain",
            "Accept-Encoding":"gzip",
        },body:bodybuffer}
    let res = await post(opt)
    if (res.code === 0){
        console.log(`[${account.nick}] ${type === 'read'?'阅读':'听书'}上传成功`)
    }else {
        console.log(`[${account.nick}] ${type === 'read'?'阅读':'听书'}上传失败，`,toJSON(res))
    }
}

async function userInfo(account) {
    let opt = signHeader('/welfare/web/task/user',account.param,populateUrlObject(''))
    let res = await get(opt)
    // console.log(opt)
    if (res.code === 0){
        console.log(`登录状态：${res.body.is_login}`)
        console.log(`金币：${res.body.coin.coin_amount}`)
        console.log(`金额：${res.body.coin.cash_amount}`)
        console.log(`账号状态：${res.body.is_black?'已锁定':'未锁定'}`)
    }else {
        console.log(`获取账号信息失败,`,toJSON(res))
    }
}

async function taskList(account) {
    let opt = signHeader('/welfare/web/task/list',account.param,populateUrlObject(''))
    let res = await get(opt)
    if (res.code === 0){
        let vlist = res.body.video_list
        if (!('undefined' == (vlist.sign_pop+''))){
            console.log(`签到视频任务次数剩余 ${vlist.sign_pop.use_status.remaining_count}`)
            account.signinvideo = vlist.sign_pop.use_status.remaining_count > 5
        }
        if (!('undefined' == (vlist.global_pop+''))){
            console.log(`广告视频任务次数剩余 ${vlist.global_pop.use_status.remaining_count}`)
            account.advideo = vlist.global_pop.use_status.remaining_count > 0
        }
        
        for (let task in res.body.task_info) {
            switch (task) {
                case 'new_login':
                    let currentDate = getDateString()
                    for (let rl of res.body.task_info.new_login.reward_list) {
                        if (currentDate === rl.date){
                            if (rl.reward_status != 2){
                                await receive(account,'新用户奖励',rl.id,1004)
                                break
                            }
                            break
                        }
                    }
                    break
                case 'sign':
                    let signobj = res.body.task_info.sign;
                    for (let d of signobj.detail_list) {
                        if (signobj.curr_day === d.days && d.sign_status === 1){
                            await receive(account,'普通签到',signobj.id,signobj.task_type,`sub_id=${d.sub_id}&task_version=${signobj.task_version}`)
                            break
                        }
                    }
                    await videoReport(account,11106,'VIDEOSIGNNEW','普通签到')
                    break
                case 'video':
                    let videoobj = res.body.task_info.video
                    if (videoobj.process.done_count < videoobj.process.total_count ){
                        await receive(account,'看视频赚金币',videoobj.id,videoobj.task_type)
                    }
                    break
                case 'box':
                    let boxobj = res.body.task_info.box
                    // if (boxobj.box_status_data.last_time === 0){
                        let boxrewardlist = boxobj.reward_list
                        await receive(account,'宝箱任务',boxobj.id,boxobj.task_type,`sub_id=${boxrewardlist[boxobj.box_status_data.count].sub_id}`)
                        await videoReport(account,11105,'VIDEO_POP_WINDOW','宝箱广告')
                    // }
                    console.log(`[${account.nick}] 宝箱已开次数：${boxobj.box_status_data.count}`)
                    break
                case 'add_bookshelf':
                    // let addobj = res.body.task_info.add_bookshelf
                    // if (addobj.done_status != 2)
                    // await receive(param,'首次加入书架',addobj.id,addobj.task_type)
                    break
                case 'new_preference_choice':
                    // let likeobj = res.body.task_info.new_preference_choice
                    // if (likeobj.done_status != 2)
                    //     await receive(param,'选阅读偏好赚0.3元',likeobj.id,likeobj.task_type)
                    break
                case 'new_greeting_gift':
                    let giftobj = res.body.task_info.new_greeting_gift
                    if (giftobj.reward_status != 2)
                        await receive(account,'新人2元见面礼',giftobj.id,giftobj.task_type)
                    break
                case 'ad_wall':
                    let adwall = res.body.task_info.ad_wall
                    if (adwall.process.done_count < adwall.process.total_count)
                        await adWallGetCount(account)
                    break
                case 'reading':
                    let reading = res.body.task_info.reading
                    account.readtime = reading.today_time_long
                    for (let reward of reading.reward_list) {
                        if (reward.done_status === 2 && reward.reward_status === 1){
                            await receive(account,'阅读奖励',reading.id,reading.task_type,`sub_id=${reward.sub_id}&task_version=${reading.task_version}`)
                            videoReport(account,11295,'VIDEO_POP_WINDOW','看视频赚金币')
                        }
                    }
                    break
                case 'ting':
                    let ting = res.body.task_info.ting
                    account.tingtime = ting.today_time_long
                    for (let reward of ting.reward_list) {
                        if (reward.done_status === 2 && reward.reward_status === 1){
                            await receive(account,'听书奖励',ting.id,ting.task_type,`sub_id=${reward.sub_id}&task_version=${ting.task_version}`)
                        }
                    }

                    break
                case 'times':
                    let times = res.body.task_info.times
                    let hours = new Date().getHours();
                    if (hours === 7 || hours === 8 ){
                        if (times.item[0].done_status != 2)
                            await receive(account,'吃饭赚钱',times[0].id,times[0].task_type,`sub_type=${times[0].sub_type}`)
                    }else if (hours === 11 || hours === 12|| hours === 13){
                        if (times.item[1].done_status != 2)
                            await receive(account,'吃饭赚钱',times[1].id,times[1].task_type,`sub_type=${times[1].sub_type}`)
                    }else if (hours === 17 || hours === 18){
                        if (times.item[2].done_status != 2)
                            await receive(account,'吃饭赚钱',times[2].id,times[2].task_type,`sub_type=${times[2].sub_type}`)
                    }else if (hours === 20|| hours === 21||hours ===22){
                        if (times.item[3].done_status != 2)
                            await receive(account,'吃饭赚钱',times[3].id,times[3].task_type,`sub_type=${times[3].sub_type}`)
                    }
                    break
                    default:
                        // console.log(`未知任务:${res.body.task_info[task].main_title}`)
            }
        }





    }else {
        console.log(`获取任务列表失败,`,toJSON(res))
    }
}

async function receive(account,taskname,id,type,extstr = '') {
    let tmp = `${account.param}&id=${id}&task_type=${type}`
    if (extstr!=''){
        tmp = tmp +'&'+extstr
    }
    let opt = signHeader('/welfare/web/task/receive',tmp,populateUrlObject(''))
    let res = await get(opt)
    // console.log(opt)
    if (res.code === 0){
        console.log(`[${account.nick}] ${taskname} 任务完成`)
    }else if (res.code === 401605){
        console.log(`[${account.nick}] ${taskname} 任务完成失败,`,res.msg)
    }else {
        console.log(`[${account.nick}] ${taskname} 任务完成失败,`,toJSON(res))
    }
}

async function videoReport2(account, param, pos,taskname) {
    let ts = getTimestamp()
    const randomValue = Math.random() * 100;
    // 格式化随机数，保留两位小数
    const formattedRandomValue = randomValue.toFixed(2);
    account.param = account.param + `&ecpmVideo=${formattedRandomValue}`
    let signstr =  `timestamp=${ts}&usr=${getValue(account.param,'usr')}&param=${param}&p2=${getValue(account.param,'p2')}`
    let sign = createSign(sortUri(signstr))
    sign = sign.replaceAll(' ', '+')
    let url = `https://welfare-dj.palmestore.com/welfare/web/video/report?${account.param}&param=${param}&sign=${sign}&timestamp=${ts}&param=${param}&pos=${pos}&${account.param}`
    let baseurl = url.split('?')[0]
    // console.log(baseurl)
    let requrl = sortQueryParams(filterDuplicates(url.split('?')[1].split('&')).join('&'))
    requrl = baseurl + requrl
    // console.log(requrl)
    let tmpStr = sha256(requrl).substring(9, 25);
    // let timeNow = parseInt(xnonce,16)
    let timeNow = parseInt(new Date().getTime() / 1000 + '')
    let hdSign = sha256(tmpStr + timeNow);
    // console.log(hdSign)
    let opt = populateUrlObject(url)
    opt.headers['X-Nonce'] = timeNow
    opt.headers['X-Sign'] = hdSign
    let res = await get(opt)
    console.log(opt)
    if (res.code === 0){
        console.log(`${taskname} 视频任务提交成功，次数：${res.body.used_count}，获得金币：${res.body.gold}`)
    }else {
        console.log(`${taskname} 视频任务提交失败，`,toJSON(res))
    }


}

function filterDuplicates(arr) {
    return [...new Set(arr)];
}
async function videoReport(account, p, pos,taskname) {
    if (hkxsapptx){
        return
    }
    if((!account.signinvideo && taskname === '普通签到')|| !account.advideo){
        return
    }
    let param = `video_id=${p}&pos=${pos}&tactics_id=&reward_ecpm=110.55000305175781&${account.param}`
    let opt = signHeader('/welfare/web/video/report/v2',param,populateUrlObject(''))
    let res = await get(opt)
    if(res.code === 0){
        console.log(`[${account.nick}] ${taskname} 视频广告观看成功，获得${res.body.gold} ${res.body.used_count}/${res.body.remaining_count}`)
    }else{
        console.log(`[${account.nick}] ${taskname} 视频广告观看失败，`,toJSON(res))
    }
}

async function adWallGetCount(account) {
    let ts = getTimestamp()

    let url = `https://dj.palmestore.com/zycl/api/gold/adWallGetCount?${account.param}&positionId=11156&positionType=2`
    let body = `positionId=11156&positionType=2&usr=${account.usr}`
    let opt = populateUrlObject(url,'',body)
    opt.headers.Host = 'dj.palmestore.com'
    let res = await post(opt)
    if (res.code === 0){
        console.log(`[${account.nick}] 资讯浏览次数 ${res.body.adWallCount}`)
        await adWallReport(account)

        if(new Date().getHours() > 18){return}
        await adWallReward(account,1,50)
        await $.wait(4000)
        await adWallReward(account,2,50)


    }else {
        console.log(`[${account.nick}] 资讯信息获取失败，`,toJSON(res))
    }
}



async function adWallReport(account,adWallId = 0,rewardCount) {
    let ts = getTimestamp()
    let s = `timestamp=${ts}&usr=${account.usr}`
    let body = `reportType=1&positionId=11156&positionType=2&usr=${account.usr}&timestamp=${ts}`
    let sign = createSign(sortUri(s))
    body += `&sign=${sign}`
    let url = `https://dj.palmestore.com/zycl/api/gold/adWallReport?${account.param}&timestamp=${ts}&sign=${sign}&reportType=1&positionId=11156&positionType=2`
    let opt  = populateUrlObject(url,'',body)
    opt.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
    opt.headers.Host = 'dj.palmestore.com'
    let res = await post(opt)
    if (res.code === 0){
        console.log(`[${account.nick}] 资讯开始成功`)
    }else {
        console.log(`[${account.nick}] 资讯提交失败，`,toJSON(res))
    }
}
async function adWallReward(account,rewardType,rewardCount) {
    let ts = getTimestamp()
    let s = `timestamp=${ts}&usr=${account.usr}&positionId=11156`
    let body = `rewardCount=${rewardCount}&positionId=11156&positionType=2&usr=${account.usr}&rewardType=${rewardType}&adWallId=94&timestamp=${ts}`
    let sign = createSign(sortUri(s))
    body += `&sign=${sign}`
    let url = `https://dj.palmestore.com/zycl/api/gold/adWallReward?${account.param}&timestamp=${ts}&sign=${sign}&rewardCount=${rewardCount}&adWallId=94&positionId=11156&positionType=2}&rewardType=${rewardType}`
    let opt  = populateUrlObject(url,'',body)
    opt.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
    opt.headers.Host = 'dj.palmestore.com'
    let res = await post(opt)
    if (res.code === 0){
        console.log(`[${account.nick}] rewardType ${rewardType} 资讯提交成功`)
    }else {
        console.log(`[${account.nick}] rewardType ${rewardType} 资讯提交失败，`,res.msg)
    }
}

function getDateString() {
    let date = new Date()
    let year = date.getFullYear()
    let month = date.getMonth() + 1
    month = month < 10 ? '0' + month : month
    let day = date.getDate()
    day = day < 10 ? '0' + day : day
    return `${year}${month}${day}`
}
function getDateString2() {
    let date = new Date()
    let year = date.getFullYear()
    let month = date.getMonth() + 1
    month = month < 10 ? '0' + month : month
    let day = date.getDate()
    day = day < 10 ? '0' + day : day
    return `${year}-${month}-${day}`
}
function sortAndFilterParams(urlString) {
    const url = new URLSearchParams(urlString);
    const filteredAndSortedParams = new URLSearchParams();

    const paramsArray = [...url.entries()]
        .filter(([key, value]) => value !== '') // 过滤掉值为空的参数
        .sort((a, b) => a[0].localeCompare(b[0])); // 根据键排序

    paramsArray.forEach(([key, value]) => {
        filteredAndSortedParams.append(key, value);
    });

    return decodeURIComponent(filteredAndSortedParams.toString());
}

function sortQueryParams(urlString) {
    const url = new URLSearchParams(urlString);
    const sortedParams = [...url.entries()].sort((a, b) => a[0].localeCompare(b[0]));

    const sortedSearchParams = new URLSearchParams();
    sortedParams.forEach(param => {
        sortedSearchParams.append(param[0], param[1]);
    });

    return decodeURIComponent(sortedSearchParams.toString());
}
function objToStr(obj) {
    let arr = []
    for (let i in obj) {
        if (i === 'sign'){
            continue
        }
        arr.push(`${i}=${obj[i]}`)
    }
    arr = arr.sort()
    return arr.join('&')
}
async function gzip(data) {
    return new Promise((resolve, reject) => {
        zlib.gzip(data, function (err, buffer) {
            if (err) {
                reject(err)
            } else {
                resolve(buffer)
            }
        })
    })
}

async function ungzip(data) {
    let buff = await new Promise((resolve, reject) => {
        zlib.unzip(Buffer.from(data,'hex'), function (err, buffer) {
            if (err) {
                reject(err)
            } else {
                resolve(buffer)
            }
        })
    })
    return buff.toString('utf-8')
}

function getSignNonce(config) {
    let _a, _b, _c;
    const url = ((_a = config.url) === null || _a === void 0 ? void 0 : _a.split('?')) || [];
    let requestUri = url[0];
    if ("get" === config.method) {
        // requestUri += handlesRequestParams( qs.parse(config.params), null);
        requestUri += sortQueryParams(config.params) ;
    } else if ("post" === config.method) {
        // form 表单提交格式加密跟 get 一样
        if (((_b = config.headers['Content-Type']) === null || _b === void 0 ? void 0 : _b.toLocaleLowerCase().includes('x-www-form-urlencoded')) && typeof config.data === 'string') {
            requestUri += handlesRequestParams(__assign(__assign({}, qs.parse(config.data)), qs.parse(url[1])));
        }
        else {
            const requestJson = handlesRequestParams(qs === null || qs === void 0 ? void 0 : qs.parse(url[1])) + JSON.stringify(config.data);
            requestUri += requestJson;
        }
    }

    let tmpStr = sha256(requestUri)
    // console.log(requestUri)
    tmpStr= tmpStr.substring(9, 25)//218033494b10ad95

    let timeNow = parseInt(new Date().getTime() / 1000 + '');
    // timeNow = parseInt('64dc2f86',16)
    let nonce = timeNow.toString(16);
    let p = (sha256(tmpStr+timeNow));
    return {nonce: nonce, sign: p}
}
function getValue(str,key,split='&') {
    let reg = new RegExp(`${key}=([^${split}]*)`)
    let res = reg.exec(str)
    return res[1]
}
let __assign = function () {
    __assign = Object.assign || function __assign(t) {
        let s, i = 1, n = arguments.length;
        for (; i < n; i++) {
            s = arguments[i];
            for (const p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
function sha256(inputData){
    let hash = crypto.createHash('sha256');
// 更新哈希对象的数据
    hash.update(inputData);
// 计算哈希值
    let hashedValue = hash.digest('hex');
    return hashedValue
}
function handlesRequestParams (data, deleteEmptyVal = false) {
    let _a, _b;
    if (data === void 0) { data = {}; }
    if (deleteEmptyVal === void 0) { deleteEmptyVal = false; }
    let requestParams = '';
    // ascii码排序
    (_b = (_a = Object.keys(data || {})) === null || _a === void 0 ? void 0 : _a.sort(function (x1, x2) {
        // 跟后台统一 不转大写
        // const x1 = a?.toUpperCase();
        // const x2 = b?.toUpperCase();
        if (x1 < x2)
            return -1;
        if (x1 > x2)
            return 1;
        return 0;
    })) === null || _b === void 0 ? void 0 : _b.map(function (key, index) {
        const _value = function (symbol) {
            if (symbol === void 0) {
                symbol = '';
            }
            if (Array.isArray(data[key])) {
                // 防止出现两个一样参数  临时方案，后续会有其他解决方案
                return "".concat(symbol).concat(key, "[0]=").concat(data[key][0], "&").concat(key, "[1]=").concat(data[key][1]);
            }
            return "".concat(symbol).concat(key, "=").concat(data[key]);
        };
        // 删除空val参数
        if (deleteEmptyVal) {
            if (data[key]) {
                requestParams += index < 1 ? _value() : _value('&');
            }
        }
        else {
            requestParams += index < 1 ? _value() : _value('&');
        }
    });
    return requestParams;
};
function random(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}
async function gzip(data) {
    return new Promise((resolve, reject) => {
        zlib.gzip(data, function (err, buffer) {
            if (err) {
                reject(err)
            } else {
                resolve(buffer)
            }
        })
    })
}
function createSign(data) {
    const sign = crypto.createSign('RSA-SHA1');
    sign.update(data);
    sign.end();
    return sign.sign(privateKey).toString('base64').replaceAll(' ', '+');

}
function sortUri(uri) {
    let arr = uri.split('&')
    arr.sort()
    return arr.join('&')
}
function htmlParse(html) {
    return cheerio.load(html)
}
function toJSON(obj) {
    if (typeof obj === 'string'){
        return obj
    }else {
        try {
            return JSON.stringify(obj)
        }catch (e) {
            console.log(`对象转JSON出错 ${e}`)
        }
    }
}
function toObj(str) {
    if (typeof str === 'object'){
        return str
    }else {
        try {
            return JSON.parse(str)
        }catch (e) {
            console.log(`JSON转对象出错 ${str}------ ${e}`)
        }
    }
}
function getTimestamp() {
    let ts = new Date().getTime()
    return ts
}
async function post(opt) {
    return new Promise((resolve, reject) => {
        $.post(opt,async(err,resp,data) => {
            try {
                if (err){
                    console.log('post请求错误')
                    // console.log($.toStr(err))
                    resolve({code:1,msg:err})
                }else {
                    if (debuglog){
                        console.log(data)}
                    httpResult = $.toObj(data)
                    resolve(httpResult)
                }
            }catch (e) {
                console.log(e)
            }finally{
                resolve()
            }
        })
    })
}

async function get(opt) {
    return new Promise((resolve, reject) => {
        $.get(opt,async(err,resp,data) => {
            try {
                if (err){
                    console.log('get请求错误')
                    // console.log($.toStr(err))
                    resolve({code:1,msg:err})
                }else {
                    if (debuglog){
                        console.log(data)}
                    httpResult = $.toObj(data)
                    resolve(httpResult)
                }
            }catch (e) {
                console.log(e)
            }finally{
                resolve()
            }
        })
    })
}

function signHeader(url,param,opt,method = 'get') {
    let request = {
        method: method,
        url: url,
        params: param
    }
    if (url === '/welfare/web/video/report'){
        request.url = basehost + url
    }
    let {sign,nonce} = getSignNonce(request)

    opt.headers['x-sign'] = sign
    opt.headers['x-nonce'] = nonce
    opt.url = basehost+url+'?'+param
    return opt
}

function populateUrlObject(url,cookie='',body=''){
    // let host = (url.split('//')[1]).split('/')[0]
    let urlObject = {
        url: url,
        headers: {
            'Host': 'welfare-dj.palmestore.com',
            "user-agent":'Mozilla/5.0 (Linux; Android 10; PBEM00 Build/QKQ1.190918.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/114.0.5735.130 Mobile Safari/537.36 zyApp/sudu zyVersion/2.0.0 zyChannel/331104',
            // "Content-Type":"application/x-www-form-urlencoded; charset=UTF-8",
            "x-requested-with":"com.dj.speed",
            "Accept-Encoding":"gzip, deflate",
        },
    }
    if(body) urlObject.body = body
    if (cookie) urlObject.headers['Cookie'] = cookie
    return urlObject;
}
function Env(t, e) { class s { constructor(t) { this.env = t } send(t, e = "GET") { t = "string" == typeof t ? { url: t } : t; let s = this.get; return "POST" === e && (s = this.post), new Promise((e, i) => { s.call(this, t, (t, s, r) => { t ? i(t) : e(s) }) }) } get(t) { return this.send.call(this.env, t) } post(t) { return this.send.call(this.env, t, "POST") } } return new class { constructor(t, e) { this.name = t, this.http = new s(this), this.data = null, this.dataFile = "box.dat", this.logs = [], this.isMute = !1, this.isNeedRewrite = !1, this.logSeparator = "\n", this.startTime = (new Date).getTime(), Object.assign(this, e), this.log("", `\ud83d\udd14${this.name}, \u5f00\u59cb!`) } isNode() { return "undefined" != typeof module && !!module.exports } isQuanX() { return "undefined" != typeof $task } isSurge() { return "undefined" != typeof $httpClient && "undefined" == typeof $loon } isLoon() { return "undefined" != typeof $loon } toObj(t, e = null) { try { return JSON.parse(t) } catch { return e } } toStr(t, e = null) { try { return JSON.stringify(t) } catch { return e } } getjson(t, e) { let s = e; const i = this.getdata(t); if (i) try { s = JSON.parse(this.getdata(t)) } catch { } return s } setjson(t, e) { try { return this.setdata(JSON.stringify(t), e) } catch { return !1 } } getScript(t) { return new Promise(e => { this.get({ url: t }, (t, s, i) => e(i)) }) } runScript(t, e) { return new Promise(s => { let i = this.getdata("@chavy_boxjs_userCfgs.httpapi"); i = i ? i.replace(/\n/g, "").trim() : i; let r = this.getdata("@chavy_boxjs_userCfgs.httpapi_timeout"); r = r ? 1 * r : 20, r = e && e.timeout ? e.timeout : r; const [o, h] = i.split("@"), a = { url: `http://${h}/v1/scripting/evaluate`, body: { script_text: t, mock_type: "cron", timeout: r }, headers: { "X-Key": o, Accept: "*/*" } }; this.post(a, (t, e, i) => s(i)) }).catch(t => this.logErr(t)) } loaddata() { if (!this.isNode()) return {}; { this.fs = this.fs ? this.fs : require("fs"), this.path = this.path ? this.path : require("path"); const t = this.path.resolve(this.dataFile), e = this.path.resolve(process.cwd(), this.dataFile), s = this.fs.existsSync(t), i = !s && this.fs.existsSync(e); if (!s && !i) return {}; { const i = s ? t : e; try { return JSON.parse(this.fs.readFileSync(i)) } catch (t) { return {} } } } } writedata() { if (this.isNode()) { this.fs = this.fs ? this.fs : require("fs"), this.path = this.path ? this.path : require("path"); const t = this.path.resolve(this.dataFile), e = this.path.resolve(process.cwd(), this.dataFile), s = this.fs.existsSync(t), i = !s && this.fs.existsSync(e), r = JSON.stringify(this.data); s ? this.fs.writeFileSync(t, r) : i ? this.fs.writeFileSync(e, r) : this.fs.writeFileSync(t, r) } } lodash_get(t, e, s) { const i = e.replace(/\[(\d+)\]/g, ".$1").split("."); let r = t; for (const t of i) if (r = Object(r)[t], void 0 === r) return s; return r } lodash_set(t, e, s) { return Object(t) !== t ? t : (Array.isArray(e) || (e = e.toString().match(/[^.[\]]+/g) || []), e.slice(0, -1).reduce((t, s, i) => Object(t[s]) === t[s] ? t[s] : t[s] = Math.abs(e[i + 1]) >> 0 == +e[i + 1] ? [] : {}, t)[e[e.length - 1]] = s, t) } getdata(t) { let e = this.getval(t); if (/^@/.test(t)) { const [, s, i] = /^@(.*?)\.(.*?)$/.exec(t), r = s ? this.getval(s) : ""; if (r) try { const t = JSON.parse(r); e = t ? this.lodash_get(t, i, "") : e } catch (t) { e = "" } } return e } setdata(t, e) { let s = !1; if (/^@/.test(e)) { const [, i, r] = /^@(.*?)\.(.*?)$/.exec(e), o = this.getval(i), h = i ? "null" === o ? null : o || "{}" : "{}"; try { const e = JSON.parse(h); this.lodash_set(e, r, t), s = this.setval(JSON.stringify(e), i) } catch (e) { const o = {}; this.lodash_set(o, r, t), s = this.setval(JSON.stringify(o), i) } } else s = this.setval(t, e); return s } getval(t) { return this.isSurge() || this.isLoon() ? $persistentStore.read(t) : this.isQuanX() ? $prefs.valueForKey(t) : this.isNode() ? (this.data = this.loaddata(), this.data[t]) : this.data && this.data[t] || null } setval(t, e) { return this.isSurge() || this.isLoon() ? $persistentStore.write(t, e) : this.isQuanX() ? $prefs.setValueForKey(t, e) : this.isNode() ? (this.data = this.loaddata(), this.data[e] = t, this.writedata(), !0) : this.data && this.data[e] || null } initGotEnv(t) { this.got = this.got ? this.got : require("got"), this.cktough = this.cktough ? this.cktough : require("tough-cookie"), this.ckjar = this.ckjar ? this.ckjar : new this.cktough.CookieJar, t && (t.headers = t.headers ? t.headers : {}, void 0 === t.headers.Cookie && void 0 === t.cookieJar && (t.cookieJar = this.ckjar)) } get(t, e = (() => { })) { t.headers && (delete t.headers["Content-Type"], delete t.headers["Content-Length"]), this.isSurge() || this.isLoon() ? (this.isSurge() && this.isNeedRewrite && (t.headers = t.headers || {}, Object.assign(t.headers, { "X-Surge-Skip-Scripting": !1 })), $httpClient.get(t, (t, s, i) => { !t && s && (s.body = i, s.statusCode = s.status), e(t, s, i) })) : this.isQuanX() ? (this.isNeedRewrite && (t.opts = t.opts || {}, Object.assign(t.opts, { hints: !1 })), $task.fetch(t).then(t => { const { statusCode: s, statusCode: i, headers: r, body: o } = t; e(null, { status: s, statusCode: i, headers: r, body: o }, o) }, t => e(t))) : this.isNode() && (this.initGotEnv(t), this.got(t).on("redirect", (t, e) => { try { if (t.headers["set-cookie"]) { const s = t.headers["set-cookie"].map(this.cktough.Cookie.parse).toString(); this.ckjar.setCookieSync(s, null), e.cookieJar = this.ckjar } } catch (t) { this.logErr(t) } }).then(t => { const { statusCode: s, statusCode: i, headers: r, body: o } = t; e(null, { status: s, statusCode: i, headers: r, body: o }, o) }, t => { const { message: s, response: i } = t; e(s, i, i && i.body) })) } post(t, e = (() => { })) { if (t.body && t.headers && !t.headers["Content-Type"] && (t.headers["Content-Type"] = "application/x-www-form-urlencoded"), t.headers && delete t.headers["Content-Length"], this.isSurge() || this.isLoon()) this.isSurge() && this.isNeedRewrite && (t.headers = t.headers || {}, Object.assign(t.headers, { "X-Surge-Skip-Scripting": !1 })), $httpClient.post(t, (t, s, i) => { !t && s && (s.body = i, s.statusCode = s.status), e(t, s, i) }); else if (this.isQuanX()) t.method = "POST", this.isNeedRewrite && (t.opts = t.opts || {}, Object.assign(t.opts, { hints: !1 })), $task.fetch(t).then(t => { const { statusCode: s, statusCode: i, headers: r, body: o } = t; e(null, { status: s, statusCode: i, headers: r, body: o }, o) }, t => e(t)); else if (this.isNode()) { this.initGotEnv(t); const { url: s, ...i } = t; this.got.post(s, i).then(t => { const { statusCode: s, statusCode: i, headers: r, body: o } = t; e(null, { status: s, statusCode: i, headers: r, body: o }, o) }, t => { const { message: s, response: i } = t; e(s, i, i && i.body) }) } } time(t) { let e = { "M+": (new Date).getMonth() + 1, "d+": (new Date).getDate(), "H+": (new Date).getHours(), "m+": (new Date).getMinutes(), "s+": (new Date).getSeconds(), "q+": Math.floor(((new Date).getMonth() + 3) / 3), S: (new Date).getMilliseconds() }; /(y+)/.test(t) && (t = t.replace(RegExp.$1, ((new Date).getFullYear() + "").substr(4 - RegExp.$1.length))); for (let s in e) new RegExp("(" + s + ")").test(t) && (t = t.replace(RegExp.$1, 1 == RegExp.$1.length ? e[s] : ("00" + e[s]).substr(("" + e[s]).length))); return t } msg(e = t, s = "", i = "", r) { const o = t => { if (!t) return t; if ("string" == typeof t) return this.isLoon() ? t : this.isQuanX() ? { "open-url": t } : this.isSurge() ? { url: t } : void 0; if ("object" == typeof t) { if (this.isLoon()) { let e = t.openUrl || t.url || t["open-url"], s = t.mediaUrl || t["media-url"]; return { openUrl: e, mediaUrl: s } } if (this.isQuanX()) { let e = t["open-url"] || t.url || t.openUrl, s = t["media-url"] || t.mediaUrl; return { "open-url": e, "media-url": s } } if (this.isSurge()) { let e = t.url || t.openUrl || t["open-url"]; return { url: e } } } }; this.isMute || (this.isSurge() || this.isLoon() ? $notification.post(e, s, i, o(r)) : this.isQuanX() && $notify(e, s, i, o(r))); let h = ["", "==============\ud83d\udce3\u7cfb\u7edf\u901a\u77e5\ud83d\udce3=============="]; h.push(e), s && h.push(s), i && h.push(i), console.log(h.join("\n")), this.logs = this.logs.concat(h) } log(...t) { t.length > 0 && (this.logs = [...this.logs, ...t]), console.log(t.join(this.logSeparator)) } logErr(t, e) { const s = !this.isSurge() && !this.isQuanX() && !this.isLoon(); s ? this.log("", `\u2757\ufe0f${this.name}, \u9519\u8bef!`, t.stack) : this.log("", `\u2757\ufe0f${this.name}, \u9519\u8bef!`, t) } wait(t) { return new Promise(e => setTimeout(e, t)) } done(t = {}) { const e = (new Date).getTime(), s = (e - this.startTime) / 1e3; this.log("", `\ud83d\udd14${this.name}, \u7ed3\u675f! \ud83d\udd5b ${s} \u79d2`), this.log(), (this.isSurge() || this.isQuanX() || this.isLoon()) && $done(t) } }(t, e) }
