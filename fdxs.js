/**
 * 疯读小说
 *
 * cron:10 6-23/2 * * *
 * const $ = new Env('疯读小说')
 */

const $ = new Env("疯读小说");
let fdxstoken = $.isNode() ? (process.env.fdxstoken ? process.env.fdxstoken : "") : ($.getdata('fdxstoken') ? $.getdata('fdxstoken') : "")
// const notify = $.isNode() ? require('./sendNotify') : '';
const crypto = require('crypto');
const CryptoJS = require("crypto-js");
const moment = require("moment/moment");
let debug = 0
let aeskey = 'r1Ccrw0hQXY9pHGWcTcFT1HF7sMiIHFtav4XUMiqIKk='
let fdxstokenArr = []
!(async () => {
    fdxstoken = '239c2940-f967-4762-baa1-d493bf8ecf2e&73c7e1cd-cb76-4285-b50f-297657e17941&48d24e4c-a237-4ae3-82af-17ec708d3c37&d5494e86-f1ed-4156-8b21-ed91fae844dd&fdcff2ac-c9d4-4124-b535-b3ec3c1f8225'
    Envs()
    if (fdxstokenArr.length === 0){
        console.log(`没有token`)
        return
    }
    let fdxsarr = []
    for (let i = 0; i < fdxstokenArr.length; i++) {
        fdxsarr.push({"token":fdxstokenArr[i],"index":i+1})
    }


    console.log(`共${fdxsarr.length}个账号`)
    for (let i = 0; i < fdxsarr.length; i++) {
        let fdxs = fdxsarr[i]
        // await withdraw(fdxs,9)
        await tasklist(fdxs)
        // await doneTask(fdxs,'100580')
        // await getCashAccount(fdxs)
        // await userinfo(fdxs)
        console.log('-----------------')

        // await doneTask(fdxs,'83352232')
        // await doneTask(fdxs,'7104261')
        // await doneTask(fdxs,'5054741')
    }
    // await $.wait(60000*5)

    // await doneTask({"token":"239c2940-f967-4762-baa1-d493bf8ecf2e"},'83352232')

    // await doneTask({"token":"239c2940-f967-4762-baa1-d493bf8ecf2e"},'7104261')
    // await doneTask({"token":"239c2940-f967-4762-baa1-d493bf8ecf2e"},'5054741')
    // 83352232 疯读日报领金币 //5054741 看精彩书籍

})()
    .catch((e) => $.logErr(e))
    .finally(() => $.done())

function Envs() {
    if (fdxstoken.indexOf('@') > -1){
        fdxstokenArr = fdxstoken.split('@')
    }else if (fdxstoken.indexOf('&') > -1){
        fdxstokenArr = fdxstoken.split('&')
    }else if (fdxstoken.indexOf('\n') > -1){
        fdxstokenArr = fdxstoken.split('\n')
    }else {
        fdxstokenArr = [fdxstoken]
    }
}

async function userinfo(user) {
    let u = `https://ws2.fengduxiaoshuo.com/doReader/handle_user_info?_token=${user.token}&no_login_user_id=&version=v9&_sv=v1&_ts=${getTimestamp()}`
    u = urlsign(u)
    let opt = buildURLObj(u)
    opt.headers['Accept-Encoding'] = 'gzip'
    opt.headers['User-Agent'] = 'okhttp/3.12.11'
    opt.headers['Host'] = 'fiction.fengduxiaoshuo.com'
    delete opt.headers['Content-Type']
    let res = await get(opt)
    if(res.result_code === 2000){
        console.log(`账号 [${user.index}] ${res.result.audit_profile.user_name} ${res.result.wechatBound?'已绑定微信':'未绑定微信'} ${res.result.listen_time}`)
        if (res.result.wechatBound === 1){
            await withdraw(user,9)
        }else{
            await withdraw(user,9,'alipay')
        }
    }else {
        console.log(`账号 [${user.index}] 获取用户信息错误，${toJSON(res)}`)
    }
}

async function getCashAccount(user){
    let u = `https://fiction.fengduxiaoshuo.com/doReader/cash/get_cash_account_info?action_type=wechat&api_version=v3&_token=${user.token}`
    let opt = buildURLObj(u)
    let res = await get(opt)
    if(res.result_code === 2000){
        console.log(`账号 [${user.index}] 现金 ${res.result.cash}`)
        if(res.result.account_info?.alipay_nick_name !== undefined){
            console.log(`账号 [${user.index}] 支付宝绑定 ${res.result.account_info?.alipay_nick_name}`)
            // await withdraw(user,9,'alipay')
        }else if(res.result.account_info?.nick_name !== undefined){
            console.log(`账号 [${user.index}] 微信绑定 ${res.result.account_info?.nick_name}`)
            // await withdraw(user,9)
        }
    }else{
        console.log(`账号 [${user.index}] 获取账户信息错误，${toJSON(res)}`)
    }
}

async function tasklist(user) {
    let u = `https://fiction.fengduxiaoshuo.com/a/fict/h5api/incentive/center?exp_groups=DIV_CASH_LISTEN_0107%3A2&exp_groups=DIV_CASH_OTHER_NEW_0127%3A1&exp_groups=DIV_CASH_V20_PUSH_0422%3A1&exp_groups=DIV_CASH_V25_DAILY_0610%3A1&exp_groups=DIV_CASH_V26_STORE_V2_0624%3A1&exp_groups=DIV_CASH_V27_CALENDAR_0701%3A2&exp_groups=DIV_CASH_RANK_NO_DANSHU_0812%3A1&exp_groups=DIV_SHAKE_REWARD%3A1&exp_groups=DIV_CASH_SUOPING_OPPO_1014%3A1&exp_groups=DIV_CASH_WEIXIN_1104%3A1&exp_groups=DIV_open_packet_everyday_9%3A1&exp_groups=DIV_coin_ecpm_diff_0414%3A1&exp_groups=DIV_CASH_SURPRISE_20220718%3A2&exp_groups=DIV_DOUYIN_REWARD_20220816%3A2&gender=0&api_version=cash_v8&is_H5=1&open_page=1&audio_version=audio_v3&activation_day=3&_token=${user.token}&_ts=${getTimestamp()}&_sv=v1`
    u = urlsign(u)
    let opt = buildURLObj(u)
    let res = await get(opt)
    if (res.result_code === 2000){
        console.log(`账号 [${user.index}] 今日总金币：${res.result.total_coin}，总金额：${res.result.total_cash/100} 元`)
        console.log(`账号 [${user.index}] 阅读：${formatSeconds(res.result.read_time)}，听书：${formatSeconds(res.result.daily_listen_time)}`)
        let tasks = res.result.tasks
        if (!res.result.sign_in.is_sign_in_today){
            await signin(user)
        }
        //if (!res.result.quick_withdraw.today_has_sign){
       //     await quicksignin(user)
      //  }
        for (let i = 0; i < tasks.open_envelope_task.length; i++) {
            if (tasks.open_envelope_task[i].taskStatus === 0){
                if (tasks.open_envelope_task[i].rewardNum !== 0){
                    await h5videoTask(user,tasks.open_envelope_task[i].taskId,true)
                    await $.wait(20000)
                }else {
                    await h5videoTask(user,tasks.open_envelope_task[i].taskId)
                    await $.wait(20000)
                }
            }
        }
        //摇一摇
        for (let i = 0; i < tasks.shake_tasks.length; i++) {
            if (tasks.shake_tasks[i].taskStatus === 0){
                await doneTask(user,tasks.shake_tasks[i].taskId)
            }
        }
        // for (let i = 0; i < tasks.treasure_box_tasks.length; i++) {
        //     if (tasks.treasure_box_tasks[i].usedCount !== tasks.treasure_box_tasks[i].limitCount){
        //         await box(user,tasks.treasure_box_tasks[i].taskId)
        //         await $.wait(30000)
        //         await h5videoTask(user,tasks.treasure_box_tasks[i].taskId)
        //         break
        //     }
        // }

        //金币球
        for (let i = 0; i < tasks.ad_ball_tasks.length; i++) {
            if (tasks.ad_ball_tasks[i].taskStatus === 0){
                await h5doneTask(user,tasks.ad_ball_tasks[i].taskId)
                await $.wait(20000)
            }
        }
        //逛书城
        if (tasks.bookcity_tasks[0].taskStatus === 0){
            await doneTask(user,tasks.bookcity_tasks[0].id)
        }
        //直播
        if (tasks.douyin_reward.taskStatus === 0){
            await doneTask(user,tasks.douyin_reward.taskId)
        }
        //惊喜红包
        if (tasks?.dc_surprise_rp_task != undefined &&tasks.dc_surprise_rp_task.taskStatus === 0){
            await h5videoTask(user,tasks.dc_surprise_rp_task.taskId)
        }
        //小猪
        if (tasks.piggy_task.taskStatus === 0){
            await h5doneTask(user,tasks.piggy_task.taskId)
        }
        //看视频任务
        if (tasks.watch_ad_task.taskStatus === 0){
            await h5videoTask(user,tasks.watch_ad_task.id)
        }
        //看推荐书领金币
        if (tasks.top_book_task.taskStatus === 0){
            let books = await getBookList(user)
            let book = books[random(0,books.length)]
            await recommendBookTask(user,book.bookId)
        }
        //宝箱
        for (let i = 0; i < tasks.treasure_box_tasks.length; i++) {
            if (tasks.treasure_box_tasks[i].usedCount !== tasks.treasure_box_tasks[i].limitCount){
                await changeTaskStatus(user,tasks.treasure_box_tasks[i].id,'reset')
                 await changeTaskStatus(user,tasks.treasure_box_tasks[i].id,'finish_task')
                 await changeTaskStatus(user,tasks.treasure_box_tasks[i].id,'get_reward')
            }
            await $.wait(20000)
        }
        // readingtask = res.result.reading_rank
        // if (readingtask.tasks[0].usedCount !== 10){
        //     await doneTask(user,7104261)
        // }
        // if (readingtask.tasks[1].usedCount !== 3){
        //     await doneTask(user,40936912)
        // }
        // if (readingtask.tasks[2].usedCount < 1){
        //     await doneTask(user,3043621)
        // }
        // if (readingtask.tasks[3].usedCount < 1){
        //     await doneTask(user,61432932)
        // }
        //听书需要会员
        // if (res.result.daily_listen_time < 12000){
        //     let books = await getBookList(user)
        //     let book = books[random(0,books.length)]
        //     await openbook(user,book)
        //     let total = 300
        //     for (let i = 0; i < 20; i++) {
        //         let ran = random(50,70)
        //         let time = total - ran
        //         if (time <= 0){
        //             break
        //         }
        //         let sign = listenSign(time)
        //         await synclistentime(user,book,time,sign)
        //         await $.wait(60000)
        //     }
        // }
        // if (res.result.read_time < 12000){
        //     let books = await getBookList(user)
        //     let book = books[random(0,books.length)]
        //     await openbook(user,book)
        //     for (let i = 0; i < 30; i++) {
        //         await readtime(user,book)
        //         await $.wait(60000)
        //     }
        // }

    }else {
        console.log(`获取任务列表失败，${toJSON(res)}`)
    }
}



function listenSign(str) {
    let date = moment().utcOffset(8).format('yyyyMMDD')
    return MD5_Encrypt(str + date);
}

//md5
function MD5_Encrypt(str) {
    let hash = crypto.createHash('md5')
    hash.update(str)
    return hash.digest('hex')
}


async function signin(user){
    let u = `https://fiction.fengduxiaoshuo.com/doReader/task_center/sign_in?version=cash_v8&exp_groups=DIV_CASH_LISTEN_0107%3A2&exp_groups=DIV_CASH_OTHER_NEW_0127%3A1&exp_groups=DIV_CASH_V20_PUSH_0422%3A1&exp_groups=DIV_CASH_V25_DAILY_0610%3A1&exp_groups=DIV_CASH_V26_STORE_V2_0624%3A1&exp_groups=DIV_CASH_V27_CALENDAR_0701%3A2&exp_groups=DIV_CASH_RANK_NO_DANSHU_0812%3A1&exp_groups=DIV_SHAKE_REWARD%3A1&exp_groups=DIV_CASH_SUOPING_OPPO_1014%3A1&exp_groups=DIV_CASH_WEIXIN_1104%3A1&exp_groups=DIV_open_packet_everyday_9%3A1&exp_groups=DIV_coin_ecpm_diff_0414%3A1&exp_groups=DIV_CASH_SURPRISE_20220718%3A2&exp_groups=DIV_DOUYIN_REWARD_20220816%3A2&_token=${user.token}`
    u = urlsign(u,'',true)
    let opt = buildURLObj(u,'')
    let res = await post(opt)
    if(res.result_code === 2000){
        console.log(`账号 [${user.index}] ${res.result.received_rewards.title} ${res.result.received_rewards.prize_num}`);
    }else{
        console.log(`账号 [${user.index}] 签到错误，${toJSON(res)}`);
    }
}

async function withdraw(user,id,type = 'wechat'){
    let u = `https://fiction.fengduxiaoshuo.com/a/fict/h5api/incentive/withdraw?action_type=${type}&_token=${user.token}&_ts=${getTimestamp()}&_sv=v1`
    let body = `{"withdraw_id":"${id}"}`
    u = urlsign(u,body)
    let opt = buildURLObj(u,body)
    let res = await post(opt)
    if(res.result_code === 2000){
        console.log(`账号 [${user.index}] ${res.result.cash} ${res.result.msg}`);
    }else{
        console.log(`提现错误，${toJSON(res)}`);
    }
}

async function quicksignin(user){
    let u = `https://fiction.fengduxiaoshuo.com/a/fict/api/ict/mark?_token=${user.token}&_sv=v1&_ts=${getTimestamp()}`
    let body = `{"activity":"smallWithdrawal"}`
    u = urlsign(u,body)
    let opt = buildURLObj(u,body)
    opt.headers['Content-Type'] = 'application/json; charset=utf-8'
    let res = await post(opt)
    if(res.result_code === 2000){
        console.log(`快速签到成功`);
    }else{
        console.log(`快速签到错误，${toJSON(res)}`);
    }
}
async function recommendBookTask(user,bookid) {
    let u = `https://fiction.fengduxiaoshuo.com/doReader/task_center/change_task_status?_token=${user.token}&task_ids=262&action_type=auto&book_id=${bookid}&api_version=cash_v8&_sv=v1&_ts=${getTimestamp()}`
    u = urlsign(u,'',true)
    let opt = buildURLObj(u,'')
    let res = await post(opt)
    if (res.result_code === 2000){
        console.log(`阅读推荐书籍成功`)
    }else {
        console.log(`推荐书籍任务错误，${toJSON(res)}`)
    }
}
function random(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}
async function openbook(user,book) {
    let u = `https://fiction.fengduxiaoshuo.com/doReader/api/book?_token=${user.token}&book_id=${book.bookId}&need_chapters=0&version=v2&_sv=v1&_ts=${getTimestamp()}`
    u = urlsign(u)
    let opt = buildURLObj(u)
    let res = await get(opt)
    if (res.result_code === 2000){
        console.log(`打开书籍[${book.bookTitle}]成功`)
    }else {
        console.log(`打开书籍错误，${toJSON(res)}`)
    }
}
async function getBookList(user) {
    let u = `https://fiction.fengduxiaoshuo.com/a/fict/h5api/cashIncentive/topReading?gender=0&nid=7c571a05-a6f1-4a42-ba9e-4e1f5107090d&ntu=1212317&_token=${user.token}&_ts=${getTimestamp()}&_sv=v1`
    u = urlsign(u)
    let opt = buildURLObj(u)
    let res = await get(opt)
    if (res.result_code === 2000){
        return res.result.books
    }else {
        console.log(`获取书籍错误，${toJSON(res)}`)
    }
}
async function readtime(user,book) {
    let read = random(50,70)
    let u = `https://fiction.fengduxiaoshuo.com/doReader/api/readtime?_token=${user.token}&reading_time=${read}&book_id=${book.bookId}&scene=&_sv=v1&_ts=${getTimestamp()}`
    u = urlsign(u,'',true)
    let opt = buildURLObj(u)
    let res = await post(opt)
    if (res.result_code === 2000){
        console.log(`[${book.bookTitle}]上传阅读时长成功，阅读 ${formatSeconds(res.result.today_reading_time)}`)
    }else {
        console.log(`[${book.bookTitle}]上传阅读时长错误，${toJSON(res)}`)
    }
}
async function synclistentime(user,book,time,signstr) {
    let u = `https://fiction.fengduxiaoshuo.com/doReader/task_center/sync_listen_time?_token=${user.token}&listen_time=${time}&sign=${signstr}&_sv=v1&_ts=${getTimestamp()}`
    u = urlsign(u,'',true)
    let opt = buildURLObj(u)
    let res = await post(opt)
    if (res.result_code === 2000){
        console.log(`[${book.bookTitle}]上传听书时长成功，阅读 ${formatSeconds(res.result.daily_listen_time)}`)
    }else {
        console.log(`[${book.bookTitle}]上传听书时长错误，${toJSON(res)}`)
    }
}
function formatSeconds(value) {
    let theTime = parseInt(value);// 秒
    let theTime1 = 0;// 分
    let theTime2 = 0;// 小时
    if(theTime > 60) {
        theTime1 = parseInt(theTime/60);
        theTime = parseInt(theTime%60);
        if(theTime1 > 60) {
            theTime2 = parseInt(theTime1/60);
            theTime1 = parseInt(theTime1%60);
        }
    }
    let result = ""+parseInt(theTime)+"秒";
    if(theTime1 > 0) {
        result = ""+parseInt(theTime1)+"分"+result;
    }
    if(theTime2 > 0) {
        result = ""+parseInt(theTime2)+"小时"+result;
    }
    return result;
}
async function h5doneTask(user,taskid) {
    let u = `https://fiction.fengduxiaoshuo.com/a/fict/h5api/task/multFinish?_token=${user.token}&_ts=${getTimestamp()}&_sv=v1`
    let body = `{"task_ids":[${taskid}]}`
    u = urlsign(u,body)
    let opt = buildURLObj(u,body)
    let res = await post(opt)
    if (res.result_code === 2000){
        console.log(`账号 [${user.index}] ${res.result.tasks[0].task.title} ${res.result.tasks[0].task.rewardNum}`)
    }else {
        console.log(`账号 [${user.index}] 金币球错误，${toJSON(res)}`)
    }
}

async function box(user,taskid) {
    let u = `https://fiction.fengduxiaoshuo.com/a/fict/h5api/task/finishTaskWithQueryNext?_token=${user.token}&_ts=${getTimestamp()}&_sv=v1`
    let body = `{"task_id":${taskid},"extra":"{\\"ad_base_info\\":\\"M2tnNWF0MWV5ODgzMGNweNn7V6CRWLS2Q95Q6heKojHlMzKLOiAWgAJWs5NZxUNmh7fdTlDfiEjzSf+sCZVbstnsBQYbI1aZ5pPZ4N3QAv1cI5xVfSbZoNXUvrrRFv1zk+ZNRyH1v8UFS5Q1hnMZbkqqdL61aohOpGeWYuoE71s8WYvak8DO/A1AShiIbGRMAXplJjKnWiUvBANoIQA9xzDs3/hXcJurbUSLcqZxlEn/hFaDqxmq1p/WZ88XE0pnKxVV222ansdW8d7Ucn0tos/DcBrUJxtdsWJvcMP+O3Ug/pH6Ddk48BMcYi1i5EX7WCxjLOwqHSzCx/wcDlaIIMoqA94tloYFj9qG+Xtl6zBDm8hgvOch0WGYvz0oCTTUG3SsMsAECtcbyfQyMbeqSA==\\"}"}`
    u = urlsign(u,body)
    let opt = buildURLObj(u,body)
    let res = await post(opt)
    if (res.result_code === 2000){
        console.log(`${res.result.current.task.title} ${res.result.current.task.rewardNum}`)
    }else {
        console.log(`开宝箱错误，${toJSON(res)}`)
    }
}
async function changeTaskStatus(user,taskid,status) {
    let u = `https://fiction.fengduxiaoshuo.com/doReader/h5api/task_center/change_task_status?action_type=${status}&api_version=cash_v8&is_H5=1&exp_groups=DIV_CASH_LISTEN_0107%3A2&exp_groups=DIV_CASH_OTHER_NEW_0127%3A1&exp_groups=DIV_CASH_V20_PUSH_0422%3A1&exp_groups=DIV_CASH_V25_DAILY_0610%3A1&exp_groups=DIV_CASH_V26_STORE_V2_0624%3A1&exp_groups=DIV_CASH_V27_CALENDAR_0701%3A2&exp_groups=DIV_CASH_RANK_NO_DANSHU_0812%3A1&exp_groups=DIV_SHAKE_REWARD%3A1&exp_groups=DIV_CASH_SUOPING_OPPO_1014%3A1&exp_groups=DIV_CASH_WEIXIN_1104%3A1&exp_groups=DIV_open_packet_everyday_9%3A1&exp_groups=DIV_coin_ecpm_diff_0414%3A1&exp_groups=DIV_CASH_SURPRISE_20220718%3A2&exp_groups=DIV_DOUYIN_REWARD_20220816%3A2&task_ids=${taskid}&_token=${user.token}&_ts=${getTimestamp()}&_sv=v1`
    u = urlsign(u,'',true)
    let opt = buildURLObj(u,'')
    let res = await post(opt)
    if (res.result_code === 2000){
        if (status === 'get_reward'){
            console.log(`账号 [${user.index}] ${status}任务${toJSON(res.result.winRewards)}`)
        }else {
            if (res.result.currentPoints === 0){
                // console.log(`账号 [${user.index}] ${status} 改变任务状态成功`)
            }
        }
    }else {
        console.log(`账号 [${user.index}] 改变任务状态错误，${toJSON(res)}`)
    }
}



async function doneTask(user,taskid) {
    let u = `https://fiction.fengduxiaoshuo.com/a/fict/api/task/multFinish?_token=${user.token}&_sv=v1&_ts=${getTimestamp()}`
    let body = `{"task_ids":[${taskid}]}`
    u = urlsign(u,body)
    let opt = buildURLObj(u,body)
    opt.headers['Content-Type'] = 'application/json;charset=UTF-8'
    let res = await post(opt)
    if (res.result_code === 2000){
        console.log(`账号 [${user.index}] ${res.result.tasks[0].task.title} ${res.result.tasks[0].task.rewardNum}`)
    }else {
        console.log(`账号 [${user.index}] [ ${taskid} ]任务错误，${toJSON(res)}`)
    }
}

async function h5videoTask(user,taskid,delextra = false) {
    let u = `https://fiction.fengduxiaoshuo.com/a/fict/h5api/task/multFinish?_token=${user.token}&_ts=${getTimestamp()}&_sv=v1`
    let body = `{"task_ids":["5223241"],"extra":"{\\"ad_base_info\\":\\"M2tnNWF0MWV5ODgzMGNweNn7V6CRWLS2Q95Q6heKojHlMzKLOiAWgAJWs5NZxUNmh7fdTlDfiEjzSf+sCZVbstnsBQYbI1aZ5pPZ4N3QAv1cI5xVfSbZoNXUvrrRFv1zk+ZNRyH1v8UFS5Q1hnMZbkqqdL61aohOpGeWYuoE71s8WYvak8DO/A1AShiIbGRMAXplJjKnWiUvBANoIQA9xzDs3/hXcJurbUSLcqZxlEn/hFaDqxmq1p/WZ88XE0pnKxVV222ansdW8d7Ucn0tos/DcBrUJxtdsWJvcMP+O3Ug/pH6Ddk48BMcYi1i5EX7WCxjLOwqHSzCx/wcDlaIIMoqA94tloYFj9qG+Xtl6zBDm8hgvOch0WGYvz0oCTTUG3SsMsAECtcbyfQyMbeqSA==\\"}"}`
    let  adbaseinfo = {
        "tu": 202941,
        "placement_id": "946989740",
        "platform": "toutiao_reward",
        "ssp_id": 101,
        "ad_type": "reward",
        "ad_subtype": "reward",
        "s": "27B97FF295E18A349F8CC9489503A83C",
        "preset_ecpm": 95,
        "ecpm_level": 0,
        "realtime_ecpm": -1,
        "cash_ecpm": 0,
        "bid_ecpm": 95
    }
    adbaseinfo = encrypt(JSON.stringify(adbaseinfo),aeskey,'9v2hnmzyp4fpbodv')
    let body2 = `{"task_ids":["${taskid}"],"extra":"{\\"ad_base_info\\":\\"${adbaseinfo}\\"}"}`
    if (delextra){
        body2 = `{"task_ids":[${taskid}]}`
    }
    body = JSON.stringify(body)
    // console.log(body)
    // console.log(body2)
    u = urlsign(u,body2)
    let opt = buildURLObj(u,body2)
    let res = await post(opt)
    if (res.result_code === 2000){
        console.log(`账号 [${user.index}] ${res.result.tasks[0].task.title} ${res.result.tasks[0].task.rewardNum}`)
    }else {
        console.log(`账号 [${user.index}] [ ${taskid} ]视频任务错误，${toJSON(res)}`)
    }
}


function encrypt(data,key,iv) {
    key = CryptoJS.enc.Base64.parse(key);
    iv = CryptoJS.enc.Utf8.parse(iv);
    data = CryptoJS.enc.Utf8.parse(aa(data));
    let encrypted = CryptoJS.AES.encrypt(data, key, {
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.NoPadding
    });
    let finalstr = Buffer.concat([Buffer.from('9v2hnmzyp4fpbodv'),Buffer.from(encrypted.toString(),'base64')]).toString('base64')
    return finalstr
}
function aa(text) {
    let i = 32
    i = 32-(Buffer.from(text).length % 32)
    let ascii = String.fromCharCode(i)
    for (let j = 0; j < i; j++) {
        text += ascii
    }
    return text
}
function urlsign(url,body = '',signflag = false) {
    url = decodeURIComponent(url)
    body = decodeURIComponent(body)
    let signurl = ''
    if (!body){
        signurl = 'GET'
    }else {
        signurl = 'POST'
    }
    if (signflag){signurl = 'POST'}
    let path = getUrlRelativePath(url)
    let paramarr = url.split('?')[1].split('&')
    signurl = `${signurl}&${path}`
    // let arr = ['_sv','_token','_ts','_sign']
    let arr = ['_sign']
    paramarr = paramarr.filter(item => {
        if (arr.includes(item.split('=')[0])) {
            return false;
        } else {
            return true;
        }
    });
    paramarr = paramarr.sort()
    let sign = base64Encrypt(md5(`${signurl}&${paramarr.join('&')}&json=${body}&B1w2OjLnERw6fXfl`))
    sign = sign.substring(0,sign.length -2)
    return `${url}&_sign=${sign}`
}
function base64Encrypt(str) {
    // console.log(`md5 ${str}`)
    return btoa(str);
}
function getUrlRelativePath(url)
{
    var arrUrl = url.split("//");

    var start = arrUrl[1].indexOf("/");
    var relUrl = arrUrl[1].substring(start);//stop省略，截取从start开始到结尾的所有字符

    if(relUrl.indexOf("?") !== -1){
        relUrl = relUrl.split("?")[0];
    }
    return relUrl;
}
function getValue(str,key,split = '&') {
    let reg = new RegExp(`${key}=([^${split}]*)`)
    let res = reg.exec(str)
    return res[1]
}
function sha256(str) {
    let hash = crypto.createHash('sha256')
    hash.update(str)
    return hash.digest('hex')
}
function md5(str) {
    let hash = crypto.createHash('md5')
    hash.update(str)
    return hash.digest('hex')
}
function formatTime(time) {
    let date = new Date(time)
    let year = date.getFullYear()
    let month = date.getMonth() + 1
    let day = date.getDate()
    let hour = date.getHours()
    let minute = date.getMinutes()
    let second = date.getSeconds()
    return `${year}-${month}-${day} ${hour}:${minute}:${second}`
}
function toJSON(obj) {
    if (typeof obj === 'string'){
        return obj
    }else {
        try {
            return JSON.stringify(obj)
        }catch (e) {
            console.log(`对象转JSON出错 ${e}`)
        }
    }
}
function toObj(str) {
    if (typeof str === 'object'){
        return str
    }else {
        try {
            return JSON.parse(str)
        }catch (e) {
            console.log(`JSON转对象出错 ${str}------ ${e}`)
        }
    }
}
async function msg(title, text) {
    if ($.isNode()){
        await notify.sendNotify(title, text)
    }else {
        $.msg(title,'',text)
    }

}
function getTimestamp() {
    let ts = Math.floor(new Date().getTime()/1000)
    return ts
}
function buildURLObj(url,body=''){
    let host = (url.split('//')[1]).split('/')[0]
    let urlObject = {
        url: url,
        headers: {
            "Host":host,
            "User-Agent":'Mozilla/5.0 (Linux; Android 10; PBEM00 Build/QKQ1.190918.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/89.0.4389.72 MQQBrowser/6.2 TBS/046141 Mobile Safari/537.36',
            "Content-Type":"application/x-www-form-urlencoded",
        },
    }
    if(body) urlObject.body = body
    return urlObject;
}
async function post(opt) {
    return new Promise((resolve, reject) => {
        $.post(opt,async(err,resp,data) => {
            try {
                if (err){
                    console.log('post请求错误')
                    console.log($.toStr(err))
                }else {
                    if (debug){
                        console.log(data)}
                    try {
                        resolve(JSON.parse(data))
                    }catch (e) {
                        console.log(`JSON转对象出错 数据：${data}`)
                        resolve(data)
                    }
                }
            }catch (e) {
                console.log(e)
            }finally{
                resolve()
            }
        })
    })
}

async function get(opt) {
    return new Promise((resolve, reject) => {
        $.get(opt,async(err,resp,data) => {
            try {
                if (err){
                    console.log('get请求错误')
                    console.log($.toStr(err))
                }else {
                    if (debug){
                        console.log(data)}
                    // httpResult = $.toObj(data)
                    try {
                        resolve(JSON.parse(data))
                    }catch (e) {
                        console.log(`JSON转对象出错 数据：${data}`)
                        resolve(data)
                    }
                }
            }catch (e) {
                console.log(e)
            }finally{
                resolve()
            }
        })
    })
}

function Env(t, e) { class s { constructor(t) { this.env = t } send(t, e = "GET") { t = "string" == typeof t ? { url: t } : t; let s = this.get; return "POST" === e && (s = this.post), new Promise((e, i) => { s.call(this, t, (t, s, r) => { t ? i(t) : e(s) }) }) } get(t) { return this.send.call(this.env, t) } post(t) { return this.send.call(this.env, t, "POST") } } return new class { constructor(t, e) { this.name = t, this.http = new s(this), this.data = null, this.dataFile = "box.dat", this.logs = [], this.isMute = !1, this.isNeedRewrite = !1, this.logSeparator = "\n", this.startTime = (new Date).getTime(), Object.assign(this, e), this.log("", `\ud83d\udd14${this.name}, \u5f00\u59cb!`) } isNode() { return "undefined" != typeof module && !!module.exports } isQuanX() { return "undefined" != typeof $task } isSurge() { return "undefined" != typeof $httpClient && "undefined" == typeof $loon } isLoon() { return "undefined" != typeof $loon } toObj(t, e = null) { try { return JSON.parse(t) } catch { return e } } toStr(t, e = null) { try { return JSON.stringify(t) } catch { return e } } getjson(t, e) { let s = e; const i = this.getdata(t); if (i) try { s = JSON.parse(this.getdata(t)) } catch { } return s } setjson(t, e) { try { return this.setdata(JSON.stringify(t), e) } catch { return !1 } } getScript(t) { return new Promise(e => { this.get({ url: t }, (t, s, i) => e(i)) }) } runScript(t, e) { return new Promise(s => { let i = this.getdata("@chavy_boxjs_userCfgs.httpapi"); i = i ? i.replace(/\n/g, "").trim() : i; let r = this.getdata("@chavy_boxjs_userCfgs.httpapi_timeout"); r = r ? 1 * r : 20, r = e && e.timeout ? e.timeout : r; const [o, h] = i.split("@"), a = { url: `http://${h}/v1/scripting/evaluate`, body: { script_text: t, mock_type: "cron", timeout: r }, headers: { "X-Key": o, Accept: "*/*" } }; this.post(a, (t, e, i) => s(i)) }).catch(t => this.logErr(t)) } loaddata() { if (!this.isNode()) return {}; { this.fs = this.fs ? this.fs : require("fs"), this.path = this.path ? this.path : require("path"); const t = this.path.resolve(this.dataFile), e = this.path.resolve(process.cwd(), this.dataFile), s = this.fs.existsSync(t), i = !s && this.fs.existsSync(e); if (!s && !i) return {}; { const i = s ? t : e; try { return JSON.parse(this.fs.readFileSync(i)) } catch (t) { return {} } } } } writedata() { if (this.isNode()) { this.fs = this.fs ? this.fs : require("fs"), this.path = this.path ? this.path : require("path"); const t = this.path.resolve(this.dataFile), e = this.path.resolve(process.cwd(), this.dataFile), s = this.fs.existsSync(t), i = !s && this.fs.existsSync(e), r = JSON.stringify(this.data); s ? this.fs.writeFileSync(t, r) : i ? this.fs.writeFileSync(e, r) : this.fs.writeFileSync(t, r) } } lodash_get(t, e, s) { const i = e.replace(/\[(\d+)\]/g, ".$1").split("."); let r = t; for (const t of i) if (r = Object(r)[t], void 0 === r) return s; return r } lodash_set(t, e, s) { return Object(t) !== t ? t : (Array.isArray(e) || (e = e.toString().match(/[^.[\]]+/g) || []), e.slice(0, -1).reduce((t, s, i) => Object(t[s]) === t[s] ? t[s] : t[s] = Math.abs(e[i + 1]) >> 0 == +e[i + 1] ? [] : {}, t)[e[e.length - 1]] = s, t) } getdata(t) { let e = this.getval(t); if (/^@/.test(t)) { const [, s, i] = /^@(.*?)\.(.*?)$/.exec(t), r = s ? this.getval(s) : ""; if (r) try { const t = JSON.parse(r); e = t ? this.lodash_get(t, i, "") : e } catch (t) { e = "" } } return e } setdata(t, e) { let s = !1; if (/^@/.test(e)) { const [, i, r] = /^@(.*?)\.(.*?)$/.exec(e), o = this.getval(i), h = i ? "null" === o ? null : o || "{}" : "{}"; try { const e = JSON.parse(h); this.lodash_set(e, r, t), s = this.setval(JSON.stringify(e), i) } catch (e) { const o = {}; this.lodash_set(o, r, t), s = this.setval(JSON.stringify(o), i) } } else s = this.setval(t, e); return s } getval(t) { return this.isSurge() || this.isLoon() ? $persistentStore.read(t) : this.isQuanX() ? $prefs.valueForKey(t) : this.isNode() ? (this.data = this.loaddata(), this.data[t]) : this.data && this.data[t] || null } setval(t, e) { return this.isSurge() || this.isLoon() ? $persistentStore.write(t, e) : this.isQuanX() ? $prefs.setValueForKey(t, e) : this.isNode() ? (this.data = this.loaddata(), this.data[e] = t, this.writedata(), !0) : this.data && this.data[e] || null } initGotEnv(t) { this.got = this.got ? this.got : require("got"), this.cktough = this.cktough ? this.cktough : require("tough-cookie"), this.ckjar = this.ckjar ? this.ckjar : new this.cktough.CookieJar, t && (t.headers = t.headers ? t.headers : {}, void 0 === t.headers.Cookie && void 0 === t.cookieJar && (t.cookieJar = this.ckjar)) } get(t, e = (() => { })) { t.headers && (delete t.headers["Content-Type"], delete t.headers["Content-Length"]), this.isSurge() || this.isLoon() ? (this.isSurge() && this.isNeedRewrite && (t.headers = t.headers || {}, Object.assign(t.headers, { "X-Surge-Skip-Scripting": !1 })), $httpClient.get(t, (t, s, i) => { !t && s && (s.body = i, s.statusCode = s.status), e(t, s, i) })) : this.isQuanX() ? (this.isNeedRewrite && (t.opts = t.opts || {}, Object.assign(t.opts, { hints: !1 })), $task.fetch(t).then(t => { const { statusCode: s, statusCode: i, headers: r, body: o } = t; e(null, { status: s, statusCode: i, headers: r, body: o }, o) }, t => e(t))) : this.isNode() && (this.initGotEnv(t), this.got(t).on("redirect", (t, e) => { try { if (t.headers["set-cookie"]) { const s = t.headers["set-cookie"].map(this.cktough.Cookie.parse).toString(); this.ckjar.setCookieSync(s, null), e.cookieJar = this.ckjar } } catch (t) { this.logErr(t) } }).then(t => { const { statusCode: s, statusCode: i, headers: r, body: o } = t; e(null, { status: s, statusCode: i, headers: r, body: o }, o) }, t => { const { message: s, response: i } = t; e(s, i, i && i.body) })) } post(t, e = (() => { })) { if (t.body && t.headers && !t.headers["Content-Type"] && (t.headers["Content-Type"] = "application/x-www-form-urlencoded"), t.headers && delete t.headers["Content-Length"], this.isSurge() || this.isLoon()) this.isSurge() && this.isNeedRewrite && (t.headers = t.headers || {}, Object.assign(t.headers, { "X-Surge-Skip-Scripting": !1 })), $httpClient.post(t, (t, s, i) => { !t && s && (s.body = i, s.statusCode = s.status), e(t, s, i) }); else if (this.isQuanX()) t.method = "POST", this.isNeedRewrite && (t.opts = t.opts || {}, Object.assign(t.opts, { hints: !1 })), $task.fetch(t).then(t => { const { statusCode: s, statusCode: i, headers: r, body: o } = t; e(null, { status: s, statusCode: i, headers: r, body: o }, o) }, t => e(t)); else if (this.isNode()) { this.initGotEnv(t); const { url: s, ...i } = t; this.got.post(s, i).then(t => { const { statusCode: s, statusCode: i, headers: r, body: o } = t; e(null, { status: s, statusCode: i, headers: r, body: o }, o) }, t => { const { message: s, response: i } = t; e(s, i, i && i.body) }) } } time(t) { let e = { "M+": (new Date).getMonth() + 1, "d+": (new Date).getDate(), "H+": (new Date).getHours(), "m+": (new Date).getMinutes(), "s+": (new Date).getSeconds(), "q+": Math.floor(((new Date).getMonth() + 3) / 3), S: (new Date).getMilliseconds() }; /(y+)/.test(t) && (t = t.replace(RegExp.$1, ((new Date).getFullYear() + "").substr(4 - RegExp.$1.length))); for (let s in e) new RegExp("(" + s + ")").test(t) && (t = t.replace(RegExp.$1, 1 == RegExp.$1.length ? e[s] : ("00" + e[s]).substr(("" + e[s]).length))); return t } msg(e = t, s = "", i = "", r) { const o = t => { if (!t) return t; if ("string" == typeof t) return this.isLoon() ? t : this.isQuanX() ? { "open-url": t } : this.isSurge() ? { url: t } : void 0; if ("object" == typeof t) { if (this.isLoon()) { let e = t.openUrl || t.url || t["open-url"], s = t.mediaUrl || t["media-url"]; return { openUrl: e, mediaUrl: s } } if (this.isQuanX()) { let e = t["open-url"] || t.url || t.openUrl, s = t["media-url"] || t.mediaUrl; return { "open-url": e, "media-url": s } } if (this.isSurge()) { let e = t.url || t.openUrl || t["open-url"]; return { url: e } } } }; this.isMute || (this.isSurge() || this.isLoon() ? $notification.post(e, s, i, o(r)) : this.isQuanX() && $notify(e, s, i, o(r))); let h = ["", "==============\ud83d\udce3\u7cfb\u7edf\u901a\u77e5\ud83d\udce3=============="]; h.push(e), s && h.push(s), i && h.push(i), console.log(h.join("\n")), this.logs = this.logs.concat(h) } log(...t) { t.length > 0 && (this.logs = [...this.logs, ...t]), console.log(t.join(this.logSeparator)) } logErr(t, e) { const s = !this.isSurge() && !this.isQuanX() && !this.isLoon(); s ? this.log("", `\u2757\ufe0f${this.name}, \u9519\u8bef!`, t.stack) : this.log("", `\u2757\ufe0f${this.name}, \u9519\u8bef!`, t) } wait(t) { return new Promise(e => setTimeout(e, t)) } done(t = {}) { const e = (new Date).getTime(), s = (e - this.startTime) / 1e3; this.log("", `\ud83d\udd14${this.name}, \u7ed3\u675f! \ud83d\udd5b ${s} \u79d2`), this.log(), (this.isSurge() || this.isQuanX() || this.isLoon()) && $done(t) } }(t, e) }
