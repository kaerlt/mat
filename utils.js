const qs = require('qs')
const crypto = require("crypto");
var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
function sha256(inputData){
    let hash = crypto.createHash('sha256');
    hash.update(inputData);
    let hashedValue = hash.digest('hex');
    return hashedValue
}

const Sign = /** @class */ (function () {
    function Sign() {
    }

    Sign.handlesRequestParams = function (data, deleteEmptyVal) {
        var _a, _b;
        if (data === void 0) {
            data = {};
        }
        if (deleteEmptyVal === void 0) {
            deleteEmptyVal = false;
        }
        var requestParams = '';
        // ascii码排序
        (_b = (_a = Object.keys(data || {})) === null || _a === void 0 ? void 0 : _a.sort(function (x1, x2) {
            // 跟后台统一 不转大写
            // const x1 = a?.toUpperCase();
            // const x2 = b?.toUpperCase();
            if (x1 < x2)
                return -1;
            if (x1 > x2)
                return 1;
            return 0;
        })) === null || _b === void 0 ? void 0 : _b.map(function (key, index) {
            var _value = function (symbol) {
                if (symbol === void 0) {
                    symbol = '';
                }
                if (Array.isArray(data[key])) {
                    // 防止出现两个一样参数  临时方案，后续会有其他解决方案
                    return "".concat(symbol).concat(key, "[0]=").concat(data[key][0], "&").concat(key, "[1]=").concat(data[key][1]);
                }
                return "".concat(symbol).concat(key, "=").concat(data[key]);
            };
            // 删除空val参数
            if (deleteEmptyVal) {
                if (data[key]) {
                    requestParams += index < 1 ? _value() : _value('&');
                }
            } else {
                requestParams += index < 1 ? _value() : _value('&');
            }
        });
        return requestParams;
    };
    /**
     * 签名方式
     * @param config  axios 请求头参数集合
     */
    Sign.getSignNonce = function (config) {
        var _a, _b, _c;
        var url = ((_a = config.url) === null || _a === void 0 ? void 0 : _a.split('?')) || [];
        var requestUri = url[0];
        if (config.method === 'get') {
            requestUri += this.handlesRequestParams(__assign(__assign({}, config.params), qs === null || qs === void 0 ? void 0 : qs.parse(url[1])));
        }
        if (config.method === 'post') {
            // form 表单提交格式加密跟 get 一样
            if (((_b = config.headers['Content-Type']) === null || _b === void 0 ? void 0 : _b.toLocaleLowerCase().includes('x-www-form-urlencoded')) && typeof config.data === 'string') {
                requestUri += this.handlesRequestParams(__assign(__assign({}, qs.parse(config.data)), qs.parse(url[1])));
            } else {
                var requestJson = this.handlesRequestParams(qs === null || qs === void 0 ? void 0 : qs.parse(url[1])) + JSON.stringify(config.data);
                requestUri += requestJson;
            }
        }
        console.log(requestUri)
        var tmpStr = (_c = sha256(requestUri)) === null || _c === void 0 ? void 0 : _c.substring(9, 25); // 截取9到25之间的
        console.log(tmpStr)
        var timeNow = parseInt(new Date().getTime() / 1000 + ''); // 秒级
        // var timeNow = 1692151686
        var nonce = timeNow.toString(16);
        var sign = sha256(tmpStr + timeNow);
        config.headers['X-Nonce'] = nonce;
        config.headers['X-Sign'] = sign;

        return {nonce: nonce, sign: sign};
    };

    return Sign;
}());

module.exports = {Sign}